import express = require('express');
import cors = require('cors');
import bodyParser = require('body-parser');
import { BbtMenu } from './bbt';
let app = express(); //Call express factory to create an 'app'
//An app can be thought of a http server, 
//that you can add end-points to.

let orderList: Array<any> = [];

//setup app 'middleware'
//we need Cors and Body parser

var corsOptions = {
    origin: '*', //Allow all origins
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}
app.use(cors(corsOptions));
app.use(bodyParser.json()); //Parse json http bodies
app.param('store', function(req, res,next, value){
  (<any> req).data = (<any> req).data || {}; //Js magic, adding a data property
  (<any>req).data.store = value;  //JS magx, store the store
  next(); //Allows for redirection if store doesn't exist or something.
});

//Add end points
app.get("/test",function(req, res){
    //Good to have a simple one just to make
    //sure things work.
    res.send('{"test": 1 }');
    //event handler for echo endpiont
});

app.get("/menu/:store",function(req,res){
    res.header("Content-Type","application/json");
    let menu = new BbtMenu((<any>res).data.store);
     res.json(menu);
});
app.post("/order", function(req,res){
   console.log("body",req.body); //should be request body
   orderList.push(req.body);
   console.log(orderList);
   res.send({orders : orderList});
});
//Start the server
app.listen(8000,function(){
    console.log("server started");
})
console.log("Setup script finised. Notice console.log runs before the above one.");