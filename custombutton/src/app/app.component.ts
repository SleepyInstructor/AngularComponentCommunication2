import { Component, ViewChild, Directive, ViewChildren, QueryList, Input } from '@angular/core';
import { CustomButtonComponent } from './custom-button/custom-button.component';

//How use selector to choose the second button
@Directive({ selector: 'cool-button' })
export class buttonQuery {
  @Input() id: string;

};


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  //Get child with a # name, we just put the name
  //In the example #cb is in the HTML, so we use cb 
  @ViewChild("cb") children: CustomButtonComponent; //Can get reference of our element in our page

  title: string = 'app';
  text: string = 'Cool Button';
  clickCount: number = 0;

  checklist() {

  }
  clicked() {
    console.log(this.children);
    this.clickCount = this.children.count;
  }
  clickedFive(event) {
    console.log("Clicked a multiple of five times",event);
  }

}
