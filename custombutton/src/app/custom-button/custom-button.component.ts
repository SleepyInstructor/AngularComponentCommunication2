import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';

//Custom button:
//Takes in an input parameter for the string to be displayed in the button
//Has two outputs
//One is the click event, which we don't need to do anything with since it bubbles up.
//The other is count, which we have to find a way to get it out of the object.

@Component({
  selector: 'cool-button',
  templateUrl: './custom-button.component.html',
  styleUrls: ['./custom-button.component.less']
})
export class CustomButtonComponent implements OnInit {

  @Input() buttonString: string = "test"; //sent from the parent to the child, because it's a string it will be a copy
  @Output() countFive = new EventEmitter<number>();  //Parent can recieve events from the child


  count = 0;
  constructor() { }

  ngOnInit() {
  }
  clicked() {
    this.count += 1;
    if (this.count % 5 == 0) {
       this.countFive.emit(this.count); //Sends the count value
    }
  }

}
