import { CustombuttonPage } from './app.po';

describe('custombutton App', () => {
  let page: CustombuttonPage;

  beforeEach(() => {
    page = new CustombuttonPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
