import { Injectable } from '@angular/core';
import { TeaItem, OrderItem, ToppingItem } from "./item.class"
import { HttpClient, HttpHeaders } from '@angular/common/http'; //Adding http to our client


//service class, actually contains the logic of the program, including price calculations


//Represents and order, and includes methods to calculate
//prices and generate strings.
export class Order {

  toppingsList: Array<OrderItem> = [];
  tea: OrderItem = new OrderItem(); //Give it a throw away object to prevent undefined
  taxrate: number;

  //
 
  toppingString(): string {
    let temp : string ="";
    console.log("hello", this.toppingsList.length);
    for(let i =0; i<this.toppingsList.length; i++)  {
      temp = temp + this.toppingsList[i].name + " ";
      console.log(temp)
    }
    return temp;

  }
  toppingPrice(): number {
    //Sums prices of items in the array
    console.log(this.toppingsList.length);
    return (<Array<any>>this.toppingsList).reduce((prev: number, curr: OrderItem, idx: number, array: OrderItem[]) => {
      return prev + curr.price;
    }, 0);
  }
  teaPrice(): number {
    return this.tea.price;
  }
  totalPrice(): number {
    return this.toppingPrice() + this.teaPrice();
  }
  orderString(): string {
    return this.tea.name + ":" + this.toppingString();
  }
  clone(): Order{
    let copyOfOrder = new Order();
    copyOfOrder.tea = this.tea.clone();
    //Need to copy each item seperately.
     copyOfOrder.toppingsList = this.toppingsList.map((value:OrderItem)=>{
       console.log(value);
        return value.clone();
    });
    console.log(copyOfOrder.toppingsList.length);
    return copyOfOrder;
  }
  clear(){
    this.tea.name = "";
    this.tea.price =0;
    this.toppingsList.length =0;
  }
}

@Injectable()
export class BbtorderingService {

  toppingMenu: Array<ToppingItem>;
  teaMenu: Array<TeaItem>;
  currentOrder: Order = new Order();
  orderList: Array<Order> = [];
  resetComponets: Array<any> =[];
  constructor(private http: HttpClient) {
    this.toppingMenu = [new ToppingItem("Pears", 1),
    new ToppingItem("Mango stars", 2),
    new ToppingItem("Pudding", 1.5),
    new ToppingItem("Grass Jelly", 1.25),
    new ToppingItem("Coconut Jelly", 2),
    new ToppingItem("Fresh Blueberries", 2.5),
    new ToppingItem("Fresh Strawberry", 2.5)];
    this.teaMenu = [new TeaItem("Black Tea", 1),
    new TeaItem("Green Tea", 1.5),
    new TeaItem("White Tea", 1.5),
    new TeaItem("Oolong Tea", 2)]
  }
  addReset(element: any){
    this.resetComponets.push(element);
  }
  reset(){
     this.resetComponets.forEach((item)=>{
       item.reset();
     })
  }
  changeTea(tea){
    this.currentOrder.tea = tea;
  }
  changeToppings(toppings){
    this.currentOrder.toppingsList = toppings;
  }
  add(){
    let newOrder = this.currentOrder.clone();
    this.orderList.push(newOrder);
    this.currentOrder.clear();
    this.reset();
  }
}
