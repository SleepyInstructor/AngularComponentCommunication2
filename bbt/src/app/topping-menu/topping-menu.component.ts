import { Component, OnInit, ViewChild } from '@angular/core';
import { TeaMenuComponent } from '../tea-menu/tea-menu.component';
import { BbtorderingService } from '../bbtordering.service';
import { SelectionListComponent } from '../selection-list/selection-list.component';
//Component to show the topping selection
@Component({
  selector: 'topping-menu',
  templateUrl: './topping-menu.component.html',
  styleUrls: ['./topping-menu.component.css']
})
export class ToppingMenuComponent implements OnInit {
  @ViewChild(SelectionListComponent) list;
  //Only needs to inject items directly access BbtorderingService
  constructor(private teaApp: BbtorderingService) { }

  ngOnInit() {
  }
  ngAfterViewInit() {
    this.teaApp.addReset(this);
  }

  //Update Toppings
  updateToppings(toppings) {
    this.teaApp.changeToppings(toppings);
  }
  reset() {
    this.list.reset();
  }
}
