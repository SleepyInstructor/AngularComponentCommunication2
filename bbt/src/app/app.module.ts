import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ToppingMenuComponent } from './topping-menu/topping-menu.component';
import { TeaMenuComponent } from './tea-menu/tea-menu.component';
import { PriceDisplayComponent } from './price-display/price-display.component';
import { SelectionListComponent } from './selection-list/selection-list.component';
import { BbtorderingService } from './bbtordering.service';
import { RadioListComponent } from './radio-list/radio-list.component';

@NgModule({
  declarations: [
    AppComponent,
    ToppingMenuComponent,
    TeaMenuComponent,
    PriceDisplayComponent,
    SelectionListComponent,
    RadioListComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [BbtorderingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
