//Component for displaying the  the current order and all the drinks ordered so far
import { Component, OnInit } from '@angular/core';
import {BbtorderingService, Order} from '../bbtordering.service';
@Component({
  selector: 'price-display',
  templateUrl: './price-display.component.html',
  styleUrls: ['./price-display.component.css']
})
export class PriceDisplayComponent implements OnInit {

  constructor(private teaApp : BbtorderingService) { }

  ngOnInit() {
  }
  add(){
    this.teaApp.add();
  }

}
