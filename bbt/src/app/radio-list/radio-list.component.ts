import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import {TeaItem, OrderItem} from '../item.class';


@Component({
  selector: 'radio-list',
  templateUrl: './radio-list.component.html',
  styleUrls: ['./radio-list.component.less']
})
export class RadioListComponent implements OnInit {

  @ViewChild('options') pageElement : any;
  @Input() list : Array<TeaItem> =[];
  @Output() order = new EventEmitter<OrderItem>();
  constructor() { }

  ngOnInit() {
  }
  ngAfterViewInit(){

  }
  updateSelection(options : Element){
    console.log(this.pageElement);
    let radioButtons = options.querySelectorAll(".rb");

    //Find first checked item, and change property values of
    //bound order.
    for(let i = 0; i < radioButtons.length; i +=1){
       if((<any>radioButtons[i]).checked){
         this.order.emit(new OrderItem(this.list[i].name,this.list[i].price));        
          break;
       }
    }
    
  }

  //Loop through buttons, set all of them to false.
  reset(){
    let options = this.pageElement.nativeElement;
    let radioButtons =options.querySelectorAll(".rb");
    for(let i = 0; i < radioButtons.length; i +=1){
      (<any>radioButtons[i]).checked = false;     
   }
  }

}
