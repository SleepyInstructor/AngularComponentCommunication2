import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeaMenuComponent } from './tea-menu.component';

describe('TeaMenuComponent', () => {
  let component: TeaMenuComponent;
  let fixture: ComponentFixture<TeaMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeaMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeaMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
