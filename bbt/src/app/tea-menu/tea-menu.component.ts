import { Component, OnInit, ViewChild } from '@angular/core';
import { BbtorderingService } from '../bbtordering.service';
import { RadioListComponent } from '../radio-list/radio-list.component';


@Component({
  selector: 'tea-menu',
  templateUrl: './tea-menu.component.html',
  styleUrls: ['./tea-menu.component.css']
})
export class TeaMenuComponent implements OnInit {

  @ViewChild(RadioListComponent) list;
  constructor(private teaApp : BbtorderingService){}

  //Event handler that updates the services tea,
  //When the tea selection changes.
  updateTea(newTea){
    this.teaApp.changeTea(newTea);
  }
  ngOnInit() {
  }
  ngAfterViewInit(){
    //Add this to the services reset list.
     this.teaApp.addReset(this);
  }
  reset(){
     this.list.reset();
  }
}
