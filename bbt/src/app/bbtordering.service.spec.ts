import { TestBed, inject } from '@angular/core/testing';

import { BbtorderingService } from './bbtordering.service';

describe('BbtorderingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BbtorderingService]
    });
  });

  it('should be created', inject([BbtorderingService], (service: BbtorderingService) => {
    expect(service).toBeTruthy();
  }));
});
