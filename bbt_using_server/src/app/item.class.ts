//Struct classes
export class ToppingItem {
    constructor(public name: string, public price: number) { };
    clone(){
        return(this.name, this.price);
    }
  }
  export class TeaItem {
    constructor(public name: string = "", public price: number = 0) {
  
    }
    clone(){
        return new OrderItem(this.name, this.price);
    }
  }
  export class OrderItem {
    constructor(public name: string = "", public price: number = 0) { }
    clone(){
        return  new OrderItem(this.name, this.price);
        
    }
  }