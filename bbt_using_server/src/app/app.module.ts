import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ToppingMenuComponent } from './topping-menu/topping-menu.component';
import { TeaMenuComponent } from './tea-menu/tea-menu.component';
import { PriceDisplayComponent } from './price-display/price-display.component';
import { SelectionListComponent } from './selection-list/selection-list.component';
import { BbtOrderingService } from './bbtordering.service';
import { BbtOrderingServiceHttp } from './bbtorderinghttp.service';
import { RadioListComponent } from './radio-list/radio-list.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ToppingMenuComponent,
    TeaMenuComponent,
    PriceDisplayComponent,
    SelectionListComponent,
    RadioListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [{provide:  BbtOrderingService, useClass: BbtOrderingServiceHttp}], //Note that I am now providing a different service, through they will have 
                                      //will have the same interface
                                      //I should abstract a interface here, but I feel too lazy to do so
  bootstrap: [AppComponent]
})
export class AppModule { }
