import { Component } from '@angular/core';
import { BbtOrderingService } from './bbtordering.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'app';
  constructor(private bbtService :BbtOrderingService ){
  }
  ngOnInit(){
    //this.bbtService.retrieveMenu();
    this.bbtService.init();
  }
}
