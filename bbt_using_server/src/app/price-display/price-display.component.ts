//Component for displaying the  the current order and all the drinks ordered so far
import { Component, OnInit } from '@angular/core';
import {BbtOrderingService, Order} from '../bbtordering.service';
@Component({
  selector: 'price-display',
  templateUrl: './price-display.component.html',
  styleUrls: ['./price-display.component.css']
})
export class PriceDisplayComponent implements OnInit {

  constructor(private teaApp : BbtOrderingService) { }

  ngOnInit() {
  }
  add(){
    let newOrder = this.teaApp.currentOrder.clone();
    this.teaApp.addOrder(newOrder);
    this.teaApp.currentOrder.clear();
    this.teaApp.reset();
  }

}
