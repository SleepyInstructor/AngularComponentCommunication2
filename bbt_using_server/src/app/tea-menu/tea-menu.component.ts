import { Component, OnInit, ViewChild } from '@angular/core';
import { BbtOrderingService } from '../bbtordering.service';
import { RadioListComponent } from '../radio-list/radio-list.component';

@Component({
  selector: 'tea-menu',
  templateUrl: './tea-menu.component.html',
  styleUrls: ['./tea-menu.component.css']
})
export class TeaMenuComponent implements OnInit {

  @ViewChild(RadioListComponent) list;
  constructor(private teaApp : BbtOrderingService){}

  ngOnInit() {
  }
  ngAfterViewInit(){
     this.teaApp.addReset(this);
  }
  reset(){
     this.list.reset();
  }
}
