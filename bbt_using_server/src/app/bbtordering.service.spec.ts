import { TestBed, inject } from '@angular/core/testing';

import { BbtOrderingService } from './bbtordering.service';

describe('BbtorderingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BbtOrderingService]
    });
  });

  it('should be created', inject([BbtOrderingService], (service: BbtOrderingService) => {
    expect(service).toBeTruthy();
  }));
});
