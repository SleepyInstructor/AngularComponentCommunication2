import { Component, OnInit, ViewChild } from '@angular/core';
import { TeaMenuComponent } from '../tea-menu/tea-menu.component';
import { BbtOrderingService } from '../bbtordering.service';
import { SelectionListComponent } from '../selection-list/selection-list.component';
//Component to show the topping selection
@Component({
  selector: 'topping-menu',
  templateUrl: './topping-menu.component.html',
  styleUrls: ['./topping-menu.component.css']
})
export class ToppingMenuComponent implements OnInit {
  @ViewChild(SelectionListComponent) list;
  //Only needs to inject items directly access BbtorderingService
  constructor(private teaApp : BbtOrderingService) { }

  ngOnInit() {
  }
  ngAfterViewInit(){
    this.teaApp.addReset(this);
 }
  reset(){
      this.list.reset();
  }
}
