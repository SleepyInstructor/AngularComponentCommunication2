import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToppingMenuComponent } from './topping-menu.component';

describe('ToppingMenuComponent', () => {
  let component: ToppingMenuComponent;
  let fixture: ComponentFixture<ToppingMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToppingMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToppingMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
