import { Injectable } from '@angular/core';
import { TeaItem, OrderItem, ToppingItem } from "./item.class"
import { HttpRequest, HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';


//service class, actually contains the logic of the program, including price calculations


//Represents and order, and includes methods to calculate
//prices and generate strings.
export class Order {

  toppingsList: Array<OrderItem> = [];
  tea: OrderItem = new OrderItem(); //Give it a throw away object to prevent undefined
  taxrate: number;

  //
  setToppings(toppings: Array<OrderItem>) {
    this.toppingsList = toppings;
  }
  setTea(tea: OrderItem) {
    this.tea = tea;
  }
  toppingString(): string {
    let temp: string = "";
    console.log("hello", this.toppingsList.length);
    for (let i = 0; i < this.toppingsList.length; i++) {
      temp = temp + this.toppingsList[i].name + " ";
      console.log(temp)
    }
    return temp;

  }
  toppingPrice(): number {
    //Sums prices of items in the array
    console.log(this.toppingsList.length);
    return (<Array<any>>this.toppingsList).reduce((prev: number, curr: OrderItem, idx: number, array: OrderItem[]) => {
      return prev + curr.price;
    }, 0);
  }
  teaPrice(): number {
    return this.tea.price;
  }
  totalPrice(): number {
    return this.toppingPrice() + this.teaPrice();
  }
  orderString(): string {
    return this.tea.name + ":" + this.toppingString();
  }
  clone(): Order {
    let copyOfOrder = new Order();
    copyOfOrder.tea = this.tea.clone();
    //Need to copy each item seperately.
    copyOfOrder.toppingsList = this.toppingsList.map((value: OrderItem) => {
      console.log(value);
      return value.clone();
    });
    console.log(copyOfOrder.toppingsList.length);
    return copyOfOrder;
  }
  clear() {
    this.tea.name = "";
    this.tea.price = 0;
    this.toppingsList.length = 0;
  }
}


@Injectable()
export class BbtOrderingServiceHttp  {

  toppingMenu: Array<ToppingItem>;
  teaMenu: Array<TeaItem>;
  currentOrder: Order = new Order();
  orderList: Array<Order> = [];
  resetComponets: Array<any> =[];
  
  constructor(private http : HttpClient) {
    this.toppingMenu = [];
  }
  addOrder( teaOrder : Order){
    console.log(teaOrder);
    this.http.post("http://localhost:8000/order", teaOrder)
    .toPromise()
    .catch((reason)=>{
      console.log("order failed",reason);
    }).then((response : any)=>{
      console.log("response",response);

      let newOrders = response.orders.map((value)=>{
         let order = new Order();
         order.toppingsList = value.toppingsList;
         order.tea = value.tea;
         return order;
      })
      this.orderList = newOrders;
    });
  }
  addReset(element: any){
    this.resetComponets.push(element);
  }
  reset(){
     this.resetComponets.forEach((item)=>{
       item.reset();
     })
  }
  
  retrieveMenu() : Promise<any>{
    /* Angular changed their api from 4 to 5
       http.get appends current host to url
       don't use it.
    */
   let request = this.http
    .request(new HttpRequest("GET","http://localhost:8000/menu/bbt"))
    .toPromise().catch((reason)=>{
      console.log(reason);
      return reason;
    });

    
    request.then((response: any)=>{
      //debugging code
      console.log(response.body);
      return response;
    });
    return request;     

  }

  init(){
     console.log("I am in the http service");
     this.retrieveMenu().then((response: any)=>{
      this.teaMenu = response.body.teaMenu;
      this.toppingMenu = response.body.toppingMenu;
      return response;
     }).catch(()=>{})
     //Quick debugging message
  }
}
