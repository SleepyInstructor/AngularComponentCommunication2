import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ToppingItem, OrderItem } from "../item.class";
//Component for display a multi select list
//with bound input and output
@Component({
  selector: 'selection-list',
  templateUrl: './selection-list.component.html',
  styleUrls: ['./selection-list.component.css']
})
export class SelectionListComponent implements OnInit {
  @ViewChild('options') element;
  @Input() list: Array<ToppingItem> = [];
  @Input() selectedToppings: Array<OrderItem> = [];
  constructor() { }

  ngOnInit() {
  }
  updateSelection(options: Element) {
    let checkBoxes = options.querySelectorAll(".cb");
    let newList = new Array<OrderItem>();
    //empty array
    this.selectedToppings.length = 0;
    //repopulate with selected items
    for (let i = 0; i < checkBoxes.length; i += 1) {
      if ((<any>checkBoxes[i]).checked) {

        this.selectedToppings.push(new OrderItem(this.list[i].name, this.list[i].price));
      }
    }

  }
  reset() {
    let options = this.element.nativeElement;
    let checkBoxes = options.querySelectorAll(".cb");
    for (let i = 0; i < checkBoxes.length; i += 1) {
      (<any>checkBoxes[i]).checked = false;

    }
  }
}
