import { Injectable } from '@angular/core';
import { TeaItem, OrderItem, ToppingItem } from "./item.class"
import { HttpHeaders , HttpClient} from '@angular/common/http';

//service class, actually contains the logic of the program, including price calculations


//Represents and order, and includes methods to calculate
//prices and generate strings.
export class Order {

  toppingsList: Array<OrderItem> = [];
  tea: OrderItem = new OrderItem(); //Give it a throw away object to prevent undefined
  taxrate: number;

  //
  setToppings(toppings: Array<OrderItem>) {
    this.toppingsList = toppings;
  }
  setTea(tea: OrderItem) {
    this.tea = tea;
  }
  toppingString(): string {
    let temp : string ="";
    console.log("hello", this.toppingsList.length);
    for(let i =0; i<this.toppingsList.length; i++)  {
      temp = temp + this.toppingsList[i].name + " ";
      console.log(temp)
    }
    return temp;

  }
  toppingPrice(): number {
    //Sums prices of items in the array
    console.log(this.toppingsList.length);
    return (<Array<any>>this.toppingsList).reduce((prev: number, curr: OrderItem, idx: number, array: OrderItem[]) => {
      return prev + curr.price;
    }, 0);
  }
  teaPrice(): number {
    return this.tea.price;
  }
  totalPrice(): number {
    return this.toppingPrice() + this.teaPrice();
  }
  orderString(): string {
    return this.tea.name + ":" + this.toppingString();
  }
  clone(): Order{
    let copyOfOrder = new Order();
    copyOfOrder.tea = this.tea.clone();
    //Need to copy each item seperately.
     copyOfOrder.toppingsList = this.toppingsList.map((value:OrderItem)=>{
       console.log(value);
        return value.clone();
    });
    console.log(copyOfOrder.toppingsList.length);
    return copyOfOrder;
  }
  clear(){
    this.tea.name = "";
    this.tea.price =0;
    this.toppingsList.length =0;
  }
}


@Injectable()
export class BbtOrderingService {

  toppingMenu: Array<ToppingItem>;
  teaMenu: Array<TeaItem>;
  currentOrder: Order = new Order();
  orderList: Array<Order> = [];
  resetComponets: Array<any> =[];
  
  constructor() {
    this.toppingMenu = [];
  }
  addReset(element: any){
    this.resetComponets.push(element);
  }
  addOrder(newOrdeer : Order){
    this.orderList.push(newOrdeer);
  }
  reset(){
     this.resetComponets.forEach((item)=>{
       item.reset();
     })
  }
  

  init(){
     //nothing to do here 
     console.log("I am not in the regular service");
     //Quick debugging message
  }
}
