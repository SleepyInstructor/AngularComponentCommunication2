import { Component, OnInit, Input, ViewChild } from '@angular/core';
import {TeaItem, OrderItem} from '../item.class';

@Component({
  selector: 'radio-list',
  templateUrl: './radio-list.component.html',
  styleUrls: ['./radio-list.component.less']
})
export class RadioListComponent implements OnInit {

  @ViewChild('options') pageElement : any;
  @Input() list : Array<TeaItem> =[];
  @Input() order : OrderItem = new OrderItem();
  constructor() { }

  ngOnInit() {
  }
  ngAfterViewInit(){

  }
  updateSelection(options : Element){
    console.log(this.pageElement);
    let radioButtons = options.querySelectorAll(".rb");
    let order = new Array<OrderItem>();
    //empty array
    //repopulate with selected items
    for(let i = 0; i < radioButtons.length; i +=1){
       if((<any>radioButtons[i]).checked){
          this.order.name = this.list[i].name;
          this.order.price = this.list[i].price;
          break;
       }
    }
    
  }
  reset(){
    let options = this.pageElement.nativeElement;
    let radioButtons =options.querySelectorAll(".rb");
    let order = new Array<OrderItem>();
    for(let i = 0; i < radioButtons.length; i +=1){
      (<any>radioButtons[i]).checked = false;     
   }
  }

}
