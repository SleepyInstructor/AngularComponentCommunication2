//Title: CPSC 2261 web component example
//With ability to detect changes in DOM tree and Attributes
//Describtion, this is a simple number display that
//has animation built in.  It could be styled to be more fancy
//but it is just to illustrate how to create a custom
//component.

//Custom elements Notes:
//1) All custom elements must extend HTMLElement
//or a child of HTMLElement( Some other element essentially)
//Here we use child in the inheritance sense, and not then nesting/tree sense
//Why, why is this so confusing.
//3) super() or a vairation thereof must be called to invoke the
//   constructor of the parent.

//References:
//Custome Elements:https://developer.mozilla.org/en-US/docs/Web/Web_Components
//Dom mutation observer:https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver

//Example:
//This custom element is suppose to represent a cash register.
///Where the numbers slide up.

//using immediately invoked function trick to keep 
//JS namespace clean
//Old school way of doing modules.  Nothing is added to the JS namespace
//If we're using webpack, then we can just use node/typescript style export commands which would be better
(function(){
function isDigit(digit){

    return typeof(digit) == "string" && digit.length == 1 && digit >= '0' && digit <= '9'
}
function seperateUnits(dim){
    let charArray = Array.from(dim);
    console.log(charArray);
  let firstCharIndex = charArray.findIndex( (value) =>{
 
        return !isDigit(value) && value != ".";
    });
  console.log("Index",firstCharIndex);
  console.log(dim);
  console.log(typeof(dim));
    let unit = dim.substring(firstCharIndex);
    let num = dim.substring(0, firstCharIndex);

    return { number: num, unit: unit};

}

//The this keyword refers to the element
class CashRegister extends HTMLElement {

         static get observedAttributes() {return ['width', 'fontsize']; }
       //Nothing has been setup when the constructor runs
       //me need to fill in dynamic content later
       //constructor fills in the static structure of the component
        constructor(){
            //public prop
            //styleElement
            //container
            //shadow
            super();

            //Create the shadow dom
            //This is what is visible to the user
            this.shadow = this.attachShadow({ mode: 'open' });
            console.log("width",this.getAttribute('width'));
            
            //Code to add the style to the element
            //this is local to the custom element
            this.styleElement = document.createElement('style');
            this.styleElement.textContent = this.generateStyle();
            
            //Add in the initial digit display
            this.container = document.createElement("div");
            this.container.classList.add("digitContainer");
            this.generateDigits();
            this.shadow.appendChild(this.container);
            this.shadow.appendChild(this.styleElement);
       
            //code to handle changes to the DOM tree
            //needed to allow us to change the digits
            //in JavaScript when the page is running.
            //I guess we could monitor attribute changes here too
            //but since we have a custom element way of donig that
            //we will use that mechanims.
            let config = { attributes: false, childList: true, subtree: true };
            let observer = new MutationObserver(()=>{
                //whenver dom changes, remove current digit and regen
                this.emptyDigits();
                this.generateDigits();
            });
            observer.observe(this, config);

        }

        //remove the digits
        emptyDigits(){
             let digits = this.container.querySelectorAll("div");
             digits.forEach((digit)=>{
                 this.container.removeChild(digit);
             })
        }
        //Generate digits
        generateDigits(){
            const numString = Array.from(this.numberString);

            numString.forEach((value)=>{
                let digitBox = document.createElement("div");
                digitBox.classList.add("digit");
                digitBox.innerHTML = value;
                this.container.appendChild(digitBox);
            });
          
            
        }
        
        //generate styles.
        //Note that global styles do not apply to elements inside the
        //shadow dom.
        //
        generateStyle(){
            //Note this is a back tick, so it's a string literal and not a forward tick

            const width = this.width;
            let widthTemp = seperateUnits(width);
            console.log("temporary width",widthTemp)
            let height = Number(widthTemp.number)/6+widthTemp.unit;
            let animBottom = Number(widthTemp.number)/6+widthTemp.unit;
            console.log(animBottom);
            let fontsize = this.fontsize;
            //using back tick strings, we can use ${} notation
            //to do variable subsitution in our string. Just like PHP
            //and some other template languages. fewer + signs everywhere.
            return `
    
            .digitContainer {
                display : flex;
                position: relative;
                width : ${width};
                border: solid 1px black;
                padding: 0.1em;
                height: ${height};
                justify-content: space-around;
                overflow: hidden;
            }

            .digit {
                position:relative;
                border: solid 1px black;
                width : ${height};
                margin: 0.1em;
               
                text-align: center;
                font-size: ${fontsize};
                animation-name: slideup;
                animation-duration: 2s;
            }
            @keyframes slideup {
                0% { top : ${animBottom}; }
                100% { top: 0em; }
            }
           
            `
        }
        //Call backs
         connectedCallback(){
             //debugging,
             //log at element lifecycles
             //useful if we have other resources that we need
            console.log("Element is connected");
        }
        //regenerate our style when the attribute that is linked to 
        //style changes
        attributeChangedCallback(name, oldValue, newValue) {
            console.log('Attribute changed.');
            this.styleElement.textContent = this.generateStyle();
            
          }
        
        //using getters to abstract the logig of getting 
        //values and attributes, makes the rest of the program easier to read.
        get width(){
            //get the attributes, if values isn't there
            //return default. Should probably have made it
            //a constant for maintainability
            return this.getAttribute("width") || "8em";
        }
        get fontsize(){
            //get the attributes, if values isn't there
            //return default. Should probably have made it
            //a constant for maintainability
            return this.getAttribute("fontsize") || "5pt";
        }
        get numberString(){
            //returns 0.00 for invalid numbers
            //other wise returns the number string with
            //two decimal places.
            let firstLabel = this.querySelector('price');
            let numberText = firstLabel ? firstLabel.innerText : "0";
            let num =  Number(numberText);
            return !isNaN(num) ? num.toFixed(2) : (0).toFixed(2);
        }
        
}

//register element to our page, so it can be used.
customElements.define('cash-register', CashRegister);
})(); //Notice the () is important, it invokes the function immediately!

