/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./worker/worker.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./worker/physics.ts":
/*!***************************!*\
  !*** ./worker/physics.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.__esModule = true;
;
;
//Check if two circles overlap
function circleCollision(ball1, ball2) {
    var deltaX = ball2.x - ball1.x;
    var deltaY = ball2.y - ball1.y;
    var distance = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2)); // '**' is the power operator 
    var touchingDistance = ball1.radius + ball2.radius;
    return distance <= touchingDistance;
}
;
function vparallel(actionLine, ball) {
    var sum = actionLine.x * ball.vx + actionLine.y * ball.vy;
    return {
        vx: actionLine.x * sum,
        vy: actionLine.y * sum
    };
}
function vperpendicular(actionLine, ball) {
    var vpara = vparallel(actionLine, ball);
    return {
        vx: ball.vx - vpara.vx,
        vy: ball.vy - vpara.vy
    };
}
//Exchange v along the line of action of the two balls.
function momentumTransfer(ball1, ball2) {
    var deltaX = ball1.x - ball2.x;
    var deltaY = ball1.y - ball2.y;
    var distance = Math.max(Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2)), 0.1); //avoid nan
    var unitVector = { x: deltaX / distance, y: deltaY / distance };
    var vpara1 = vparallel(unitVector, ball1);
    var vpara2 = vparallel(unitVector, ball2);
    var vperp1 = vperpendicular(unitVector, ball2);
    var vperp2 = vperpendicular(unitVector, ball2);
    //Balls exhange parallel components
    ball1.vx = vperp1.vx + vpara2.vx;
    ball2.vx = vperp2.vx + vpara1.vx;
    ball1.vy = vperp1.vy + vpara2.vy;
    ball2.vy = vperp2.vy + vpara1.vy;
}
var Ball = /** @class */ (function () {
    function Ball(x, y, radius, vx, vy) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.vx = vx;
        this.vy = vy;
    }
    ;
    Ball.prototype.clone = function () {
        return new Ball(this.x, this.y, this.radius, this.vx, this.vy);
    };
    //Does the physics when two circles collide
    Ball.prototype.collide = function (target) {
        //Check if balls collide
        if (circleCollision(this, target)) {
            //Do transfer of momentum
            momentumTransfer(this, target);
        }
    };
    //Does the position update when two balls collide.
    Ball.prototype.update = function (timestep) {
        this.x += timestep * this.vx;
        this.y += timestep * this.vy;
    };
    Ball.prototype.reflectX = function () {
        this.vx = -this.vx;
    };
    Ball.prototype.reflectY = function () {
        this.vy = -this.vy;
    };
    return Ball;
}());
exports.Ball = Ball;
;
var Box = /** @class */ (function () {
    function Box(maxX, maxY) {
        this.maxX = maxX;
        this.maxY = maxY;
        this.balls = [];
    }
    Box.prototype.processSim = function (timeStep) {
        this.updateAll(timeStep);
        this.checkAllCollisions();
        this.checkBounds();
    };
    Box.prototype.addBall = function (ball) {
        //add by reference to the array, if it's not already in there
        if (!this.balls.find(function (value) { return value === ball; })) {
            this.balls.push(ball);
        }
    };
    Box.prototype.updateAll = function (timeStep) {
        this.balls.forEach(function (ball) {
            ball.update(timeStep);
        });
    };
    Box.prototype.checkAllCollisions = function () {
        //This is about the only time an indexed for loop makes sense
        for (var i = 0; i < this.balls.length; i += 1) {
            for (var j = i + 1; j < this.balls.length; j += 1) {
                this.balls[i].collide(this.balls[j]);
            }
        }
    };
    Box.prototype.checkBounds = function () {
        var _this = this;
        this.balls.forEach(function (ball) {
            //I guess this could have been a helper function
            if (ball.x < 0 || ball.x > _this.maxX) {
                ball.reflectX();
                ball.x = Math.max(ball.x, 0);
                ball.x = Math.min(_this.maxX, ball.x);
            }
            if (ball.y < 0 || ball.y > _this.maxY) {
                ball.reflectY();
                ball.y = Math.max(ball.y, 0);
                ball.y = Math.min(_this.maxY, ball.y);
            }
        });
    };
    return Box;
}());
exports.Box = Box;


/***/ }),

/***/ "./worker/worker.ts":
/*!**************************!*\
  !*** ./worker/worker.ts ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.__esModule = true;
var physics = __webpack_require__(/*! ./physics */ "./worker/physics.ts");
//set onmessage to receive messages
//pstMesaage to send messages
var box = undefined;
var timeStep = 75;
onmessage = function (message) {
    console.log(message.data);
    if (!box) {
        box = new physics.Box(message.data.x, message.data.y);
        message.data.circles.forEach(function (ball) {
            var vx = 0.1 * Math.random();
            var vy = 0.1 * Math.random();
            box.addBall(new physics.Ball(ball.x, ball.y, ball.radius, vx, vy));
        });
        //send message every second
        setInterval(function () {
            box.processSim(timeStep);
            postMessage(box, undefined);
        }, timeStep); //update every 100 ms.
    }
    //ignore
};


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vd29ya2VyL3BoeXNpY3MudHMiLCJ3ZWJwYWNrOi8vLy4vd29ya2VyL3dvcmtlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxrREFBMEMsZ0NBQWdDO0FBQzFFO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZ0VBQXdELGtCQUFrQjtBQUMxRTtBQUNBLHlEQUFpRCxjQUFjO0FBQy9EOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpREFBeUMsaUNBQWlDO0FBQzFFLHdIQUFnSCxtQkFBbUIsRUFBRTtBQUNySTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOzs7QUFHQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7QUMzRUMsQ0FBQztBQUtELENBQUM7QUFNRiw4QkFBOEI7QUFDOUIseUJBQXlCLEtBQWEsRUFBRSxLQUFhO0lBQ2pELElBQUksTUFBTSxHQUFHLEtBQUssQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUMvQixJQUFJLE1BQU0sR0FBRyxLQUFLLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDL0IsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFNLEVBQUksQ0FBQyxJQUFHLGVBQU0sRUFBSSxDQUFDLEVBQUMsQ0FBQywrQkFBOEI7SUFDbEYsSUFBSSxnQkFBZ0IsR0FBRyxLQUFLLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUM7SUFDbkQsT0FBTyxRQUFRLElBQUksZ0JBQWdCLENBQUM7QUFFeEMsQ0FBQztBQUFBLENBQUM7QUFFRixtQkFBbUIsVUFBb0IsRUFBRSxJQUFjO0lBQ25ELElBQUksR0FBRyxHQUFHLFVBQVUsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsR0FBSSxVQUFVLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUM7SUFDM0QsT0FBTztRQUNILEVBQUUsRUFBRSxVQUFVLENBQUMsQ0FBQyxHQUFHLEdBQUc7UUFDdEIsRUFBRSxFQUFFLFVBQVUsQ0FBQyxDQUFDLEdBQUcsR0FBRztLQUN6QjtBQUVMLENBQUM7QUFDRCx3QkFBd0IsVUFBb0IsRUFBRSxJQUFjO0lBQ3hELElBQUksS0FBSyxHQUFHLFNBQVMsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDeEMsT0FBTztRQUNILEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRSxHQUFHLEtBQUssQ0FBQyxFQUFFO1FBQ3RCLEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRSxHQUFHLEtBQUssQ0FBQyxFQUFFO0tBQ3pCLENBQUM7QUFDTixDQUFDO0FBQ0QsdURBQXVEO0FBQ3ZELDBCQUEwQixLQUFXLEVBQUUsS0FBVztJQUM5QyxJQUFJLE1BQU0sR0FBRyxLQUFLLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDL0IsSUFBSSxNQUFNLEdBQUcsS0FBSyxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBQy9CLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFNLEVBQUksQ0FBQyxJQUFHLGVBQU0sRUFBSSxDQUFDLEVBQUMsRUFBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLFdBQVc7SUFFOUUsSUFBSSxVQUFVLEdBQUcsRUFBRSxDQUFDLEVBQUUsTUFBTSxHQUFHLFFBQVEsRUFBRSxDQUFDLEVBQUUsTUFBTSxHQUFHLFFBQVEsRUFBRSxDQUFDO0lBRWhFLElBQUksTUFBTSxHQUFHLFNBQVMsQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDMUMsSUFBSSxNQUFNLEdBQUcsU0FBUyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUMxQyxJQUFJLE1BQU0sR0FBRyxjQUFjLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQy9DLElBQUksTUFBTSxHQUFHLGNBQWMsQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFFL0MsbUNBQW1DO0lBQ25DLEtBQUssQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDO0lBQ2pDLEtBQUssQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDO0lBRWpDLEtBQUssQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDO0lBQ2pDLEtBQUssQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDO0FBQ3JDLENBQUM7QUFHRDtJQUNJLGNBQW1CLENBQVMsRUFBUyxDQUFTLEVBQVMsTUFBYyxFQUFTLEVBQVUsRUFBUyxFQUFVO1FBQXhGLE1BQUMsR0FBRCxDQUFDLENBQVE7UUFBUyxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQVMsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUFTLE9BQUUsR0FBRixFQUFFLENBQVE7UUFBUyxPQUFFLEdBQUYsRUFBRSxDQUFRO0lBQUksQ0FBQztJQUFBLENBQUM7SUFDakgsb0JBQUssR0FBTDtRQUNJLE9BQU8sSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDbkUsQ0FBQztJQUNELDJDQUEyQztJQUMzQyxzQkFBTyxHQUFQLFVBQVEsTUFBWTtRQUNoQix3QkFBd0I7UUFDeEIsSUFBSSxlQUFlLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQy9CLHlCQUF5QjtZQUN6QixnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7U0FDbEM7SUFDTCxDQUFDO0lBQ0Qsa0RBQWtEO0lBQ2xELHFCQUFNLEdBQU4sVUFBTyxRQUFnQjtRQUNuQixJQUFJLENBQUMsQ0FBQyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDO1FBQzdCLElBQUksQ0FBQyxDQUFDLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUM7SUFDakMsQ0FBQztJQUNELHVCQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBQ0QsdUJBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO0lBQ3ZCLENBQUM7SUFDTCxXQUFDO0FBQUQsQ0FBQztBQW1ERyxvQkFBSTtBQW5EUCxDQUFDO0FBRUY7SUFFSSxhQUFvQixJQUFZLEVBQVUsSUFBWTtRQUFsQyxTQUFJLEdBQUosSUFBSSxDQUFRO1FBQVUsU0FBSSxHQUFKLElBQUksQ0FBUTtRQUR0RCxVQUFLLEdBQWdCLEVBQUUsQ0FBQztJQUNrQyxDQUFDO0lBQzNELHdCQUFVLEdBQVYsVUFBVyxRQUFpQjtRQUN4QixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBQ0QscUJBQU8sR0FBUCxVQUFRLElBQVU7UUFDZCw2REFBNkQ7UUFDN0QsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQUMsS0FBSyxJQUFPLE9BQU8sS0FBSyxLQUFLLElBQUksRUFBQyxDQUFDLENBQUMsRUFBRTtZQUN4RCxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN6QjtJQUNMLENBQUM7SUFDRCx1QkFBUyxHQUFULFVBQVUsUUFBZ0I7UUFDdEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFVO1lBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDMUIsQ0FBQyxDQUFDO0lBQ04sQ0FBQztJQUNELGdDQUFrQixHQUFsQjtRQUNJLDZEQUE2RDtRQUM3RCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUMzQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQy9DLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUN4QztTQUNKO0lBQ0wsQ0FBQztJQUNELHlCQUFXLEdBQVg7UUFBQSxpQkFjQztRQWJHLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQUMsSUFBSTtZQUNwQixnREFBZ0Q7WUFDaEQsSUFBSSxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxHQUFHLEtBQUksQ0FBQyxJQUFJLEVBQUU7Z0JBQ2xDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDaEIsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQzdCLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUN4QztZQUNELElBQUksSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLENBQUMsR0FBRyxLQUFJLENBQUMsSUFBSSxFQUFFO2dCQUNsQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ2hCLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUM3QixJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDeEM7UUFDTCxDQUFDLENBQUM7SUFDTixDQUFDO0lBR0wsVUFBQztBQUFELENBQUM7QUFLUyxrQkFBRzs7Ozs7Ozs7Ozs7Ozs7O0FDNUliLDBFQUFxQztBQUNyQyxtQ0FBbUM7QUFDbkMsNkJBQTZCO0FBRTdCLElBQUksR0FBRyxHQUFHLFNBQVMsQ0FBQztBQUNwQixJQUFNLFFBQVEsR0FBRyxFQUFFLENBQUM7QUFDcEIsU0FBUyxHQUFHLFVBQUMsT0FBWTtJQUNyQixPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMxQixJQUFJLENBQUMsR0FBRyxFQUFFO1FBQ04sR0FBRyxHQUFHLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3RELE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxjQUFJO1lBQzdCLElBQUksRUFBRSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDN0IsSUFBSSxFQUFFLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUM3QixHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDdEUsQ0FBQyxDQUFDLENBQUM7UUFFSCwyQkFBMkI7UUFDM0IsV0FBVyxDQUFDO1lBQ1IsR0FBRyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN6QixXQUFXLENBQUMsR0FBRyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBRWhDLENBQUMsRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDLHNCQUFzQjtLQUN2QztJQUVELFFBQVE7QUFDWixDQUFDIiwiZmlsZSI6Indvcmtlci5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vd29ya2VyL3dvcmtlci50c1wiKTtcbiIsIi8vc2ltcGxpZnlpbmcgYXNzdW1wdGlvblxyXG4vL2V2ZXJ5dGhpbmcgaGFzIHRoZSBzYW1lIG1hc3NcclxuLy9vdGhlcndpc2UgbmVlZGVkIHRvIHRha2UgaW50byBhY2NvdW50IG1hc3NcclxuaW50ZXJmYWNlIGNpcmNsZSB7XHJcbiAgICB4OiBudW1iZXIsXHJcbiAgICB5OiBudW1iZXIsXHJcbiAgICByYWRpdXM6IG51bWJlclxyXG59O1xyXG5cclxuaW50ZXJmYWNlIHZlbG9jaXR5IHtcclxuICAgIHZ4OiBudW1iZXIsXHJcbiAgICB2eTogbnVtYmVyXHJcbn07XHJcblxyXG5pbnRlcmZhY2UgdmVjdG9yMkQge1xyXG4gICAgeDogbnVtYmVyLFxyXG4gICAgeTogbnVtYmVyXHJcbn1cclxuLy9DaGVjayBpZiB0d28gY2lyY2xlcyBvdmVybGFwXHJcbmZ1bmN0aW9uIGNpcmNsZUNvbGxpc2lvbihiYWxsMTogY2lyY2xlLCBiYWxsMjogY2lyY2xlKTogYm9vbGVhbiB7XHJcbiAgICBsZXQgZGVsdGFYID0gYmFsbDIueCAtIGJhbGwxLng7XHJcbiAgICBsZXQgZGVsdGFZID0gYmFsbDIueSAtIGJhbGwxLnk7XHJcbiAgICBsZXQgZGlzdGFuY2UgPSBNYXRoLnNxcnQoZGVsdGFYICoqIDIgKyBkZWx0YVkgKiogMik7Ly8gJyoqJyBpcyB0aGUgcG93ZXIgb3BlcmF0b3IgXHJcbiAgICBsZXQgdG91Y2hpbmdEaXN0YW5jZSA9IGJhbGwxLnJhZGl1cyArIGJhbGwyLnJhZGl1cztcclxuICAgIHJldHVybiBkaXN0YW5jZSA8PSB0b3VjaGluZ0Rpc3RhbmNlO1xyXG5cclxufTtcclxuXHJcbmZ1bmN0aW9uIHZwYXJhbGxlbChhY3Rpb25MaW5lOiB2ZWN0b3IyRCwgYmFsbDogdmVsb2NpdHkpOiB2ZWxvY2l0eSB7XHJcbiAgICBsZXQgc3VtID0gYWN0aW9uTGluZS54ICogYmFsbC52eCArICBhY3Rpb25MaW5lLnkgKiBiYWxsLnZ5O1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICB2eDogYWN0aW9uTGluZS54ICogc3VtLFxyXG4gICAgICAgIHZ5OiBhY3Rpb25MaW5lLnkgKiBzdW0sXHJcbiAgICB9XHJcblxyXG59XHJcbmZ1bmN0aW9uIHZwZXJwZW5kaWN1bGFyKGFjdGlvbkxpbmU6IHZlY3RvcjJELCBiYWxsOiB2ZWxvY2l0eSk6IHZlbG9jaXR5IHtcclxuICAgIGxldCB2cGFyYSA9IHZwYXJhbGxlbChhY3Rpb25MaW5lLCBiYWxsKTtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgdng6IGJhbGwudnggLSB2cGFyYS52eCxcclxuICAgICAgICB2eTogYmFsbC52eSAtIHZwYXJhLnZ5XHJcbiAgICB9O1xyXG59XHJcbi8vRXhjaGFuZ2UgdiBhbG9uZyB0aGUgbGluZSBvZiBhY3Rpb24gb2YgdGhlIHR3byBiYWxscy5cclxuZnVuY3Rpb24gbW9tZW50dW1UcmFuc2ZlcihiYWxsMTogYmFsbCwgYmFsbDI6IGJhbGwpIHtcclxuICAgIGxldCBkZWx0YVggPSBiYWxsMS54IC0gYmFsbDIueDtcclxuICAgIGxldCBkZWx0YVkgPSBiYWxsMS55IC0gYmFsbDIueTtcclxuICAgIGxldCBkaXN0YW5jZSA9IE1hdGgubWF4KE1hdGguc3FydChkZWx0YVggKiogMiArIGRlbHRhWSAqKiAyKSwwLjEpOyAvL2F2b2lkIG5hblxyXG4gICAgXHJcbiAgICBsZXQgdW5pdFZlY3RvciA9IHsgeDogZGVsdGFYIC8gZGlzdGFuY2UsIHk6IGRlbHRhWSAvIGRpc3RhbmNlIH07XHJcblxyXG4gICAgbGV0IHZwYXJhMSA9IHZwYXJhbGxlbCh1bml0VmVjdG9yLCBiYWxsMSk7XHJcbiAgICBsZXQgdnBhcmEyID0gdnBhcmFsbGVsKHVuaXRWZWN0b3IsIGJhbGwyKTtcclxuICAgIGxldCB2cGVycDEgPSB2cGVycGVuZGljdWxhcih1bml0VmVjdG9yLCBiYWxsMik7XHJcbiAgICBsZXQgdnBlcnAyID0gdnBlcnBlbmRpY3VsYXIodW5pdFZlY3RvciwgYmFsbDIpO1xyXG5cclxuICAgIC8vQmFsbHMgZXhoYW5nZSBwYXJhbGxlbCBjb21wb25lbnRzXHJcbiAgICBiYWxsMS52eCA9IHZwZXJwMS52eCArIHZwYXJhMi52eDtcclxuICAgIGJhbGwyLnZ4ID0gdnBlcnAyLnZ4ICsgdnBhcmExLnZ4O1xyXG5cclxuICAgIGJhbGwxLnZ5ID0gdnBlcnAxLnZ5ICsgdnBhcmEyLnZ5O1xyXG4gICAgYmFsbDIudnkgPSB2cGVycDIudnkgKyB2cGFyYTEudnk7XHJcbn1cclxuXHJcblxyXG5jbGFzcyBCYWxsIHtcclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyB4OiBudW1iZXIsIHB1YmxpYyB5OiBudW1iZXIsIHB1YmxpYyByYWRpdXM6IG51bWJlciwgcHVibGljIHZ4OiBudW1iZXIsIHB1YmxpYyB2eTogbnVtYmVyKSB7IH07XHJcbiAgICBjbG9uZSgpIHtcclxuICAgICAgICByZXR1cm4gbmV3IEJhbGwodGhpcy54LCB0aGlzLnksIHRoaXMucmFkaXVzLCB0aGlzLnZ4LCB0aGlzLnZ5KTtcclxuICAgIH1cclxuICAgIC8vRG9lcyB0aGUgcGh5c2ljcyB3aGVuIHR3byBjaXJjbGVzIGNvbGxpZGVcclxuICAgIGNvbGxpZGUodGFyZ2V0OiBCYWxsKSB7XHJcbiAgICAgICAgLy9DaGVjayBpZiBiYWxscyBjb2xsaWRlXHJcbiAgICAgICAgaWYgKGNpcmNsZUNvbGxpc2lvbih0aGlzLCB0YXJnZXQpKSB7XHJcbiAgICAgICAgICAgIC8vRG8gdHJhbnNmZXIgb2YgbW9tZW50dW1cclxuICAgICAgICAgICAgbW9tZW50dW1UcmFuc2Zlcih0aGlzLCB0YXJnZXQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC8vRG9lcyB0aGUgcG9zaXRpb24gdXBkYXRlIHdoZW4gdHdvIGJhbGxzIGNvbGxpZGUuXHJcbiAgICB1cGRhdGUodGltZXN0ZXA6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMueCArPSB0aW1lc3RlcCAqIHRoaXMudng7XHJcbiAgICAgICAgdGhpcy55ICs9IHRpbWVzdGVwICogdGhpcy52eTtcclxuICAgIH1cclxuICAgIHJlZmxlY3RYKCkge1xyXG4gICAgICAgIHRoaXMudnggPSAtdGhpcy52eDtcclxuICAgIH1cclxuICAgIHJlZmxlY3RZKCkge1xyXG4gICAgICAgIHRoaXMudnkgPSAtdGhpcy52eTtcclxuICAgIH1cclxufTtcclxuXHJcbmNsYXNzIEJveCB7XHJcbiAgICBiYWxsczogQXJyYXk8QmFsbD4gPSBbXTtcclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgbWF4WDogbnVtYmVyLCBwcml2YXRlIG1heFk6IG51bWJlcikgeyB9XHJcbiAgICBwcm9jZXNzU2ltKHRpbWVTdGVwIDogbnVtYmVyKXtcclxuICAgICAgICB0aGlzLnVwZGF0ZUFsbCh0aW1lU3RlcCk7XHJcbiAgICAgICAgdGhpcy5jaGVja0FsbENvbGxpc2lvbnMoKTsgIFxyXG4gICAgICAgIHRoaXMuY2hlY2tCb3VuZHMoKTtcclxuICAgIH1cclxuICAgIGFkZEJhbGwoYmFsbDogQmFsbCkge1xyXG4gICAgICAgIC8vYWRkIGJ5IHJlZmVyZW5jZSB0byB0aGUgYXJyYXksIGlmIGl0J3Mgbm90IGFscmVhZHkgaW4gdGhlcmVcclxuICAgICAgICBpZiAoIXRoaXMuYmFsbHMuZmluZCgodmFsdWUpID0+IHsgcmV0dXJuIHZhbHVlID09PSBiYWxsIH0pKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYmFsbHMucHVzaChiYWxsKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICB1cGRhdGVBbGwodGltZVN0ZXA6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuYmFsbHMuZm9yRWFjaCgoYmFsbDogQmFsbCkgPT4ge1xyXG4gICAgICAgICAgICBiYWxsLnVwZGF0ZSh0aW1lU3RlcCk7XHJcbiAgICAgICAgfSlcclxuICAgIH1cclxuICAgIGNoZWNrQWxsQ29sbGlzaW9ucygpIHtcclxuICAgICAgICAvL1RoaXMgaXMgYWJvdXQgdGhlIG9ubHkgdGltZSBhbiBpbmRleGVkIGZvciBsb29wIG1ha2VzIHNlbnNlXHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmJhbGxzLmxlbmd0aDsgaSArPSAxKSB7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGogPSBpICsgMTsgaiA8IHRoaXMuYmFsbHMubGVuZ3RoOyBqICs9IDEpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYmFsbHNbaV0uY29sbGlkZSh0aGlzLmJhbGxzW2pdKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIGNoZWNrQm91bmRzKCkge1xyXG4gICAgICAgIHRoaXMuYmFsbHMuZm9yRWFjaCgoYmFsbCkgPT4ge1xyXG4gICAgICAgICAgICAvL0kgZ3Vlc3MgdGhpcyBjb3VsZCBoYXZlIGJlZW4gYSBoZWxwZXIgZnVuY3Rpb25cclxuICAgICAgICAgICAgaWYgKGJhbGwueCA8IDAgfHwgYmFsbC54ID4gdGhpcy5tYXhYKSB7XHJcbiAgICAgICAgICAgICAgICBiYWxsLnJlZmxlY3RYKCk7XHJcbiAgICAgICAgICAgICAgICBiYWxsLnggPSBNYXRoLm1heChiYWxsLngsIDApO1xyXG4gICAgICAgICAgICAgICAgYmFsbC54ID0gTWF0aC5taW4odGhpcy5tYXhYLCBiYWxsLngpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChiYWxsLnkgPCAwIHx8IGJhbGwueSA+IHRoaXMubWF4WSkge1xyXG4gICAgICAgICAgICAgICAgYmFsbC5yZWZsZWN0WSgpO1xyXG4gICAgICAgICAgICAgICAgYmFsbC55ID0gTWF0aC5tYXgoYmFsbC55LCAwKTtcclxuICAgICAgICAgICAgICAgIGJhbGwueSA9IE1hdGgubWluKHRoaXMubWF4WSwgYmFsbC55KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gIFxyXG59XHJcblxyXG4vL0RlZmF1bHQgc3ltYm9sIGV4cG9ydHNcclxuLy9tYWtlcyBpdCBsb29rIGxpa2UgYSBtb2R1bGVcclxuZXhwb3J0IHtcclxuICAgIEJhbGwsIEJveFxyXG59IiwiaW1wb3J0ICogYXMgcGh5c2ljcyBmcm9tICcuL3BoeXNpY3MnO1xyXG4vL3NldCBvbm1lc3NhZ2UgdG8gcmVjZWl2ZSBtZXNzYWdlc1xyXG4vL3BzdE1lc2FhZ2UgdG8gc2VuZCBtZXNzYWdlc1xyXG5cclxubGV0IGJveCA9IHVuZGVmaW5lZDtcclxuY29uc3QgdGltZVN0ZXAgPSA3NTtcclxub25tZXNzYWdlID0gKG1lc3NhZ2U6IGFueSkgPT4ge1xyXG4gICAgY29uc29sZS5sb2cobWVzc2FnZS5kYXRhKTtcclxuICAgIGlmICghYm94KSB7XHJcbiAgICAgICAgYm94ID0gbmV3IHBoeXNpY3MuQm94KG1lc3NhZ2UuZGF0YS54LCBtZXNzYWdlLmRhdGEueSk7XHJcbiAgICAgICAgbWVzc2FnZS5kYXRhLmNpcmNsZXMuZm9yRWFjaChiYWxsID0+IHtcclxuICAgICAgICAgICAgbGV0IHZ4ID0gMC4xICogTWF0aC5yYW5kb20oKTtcclxuICAgICAgICAgICAgbGV0IHZ5ID0gMC4xICogTWF0aC5yYW5kb20oKTtcclxuICAgICAgICAgICAgYm94LmFkZEJhbGwobmV3IHBoeXNpY3MuQmFsbChiYWxsLngsIGJhbGwueSwgYmFsbC5yYWRpdXMsIHZ4LCB2eSkpXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIC8vc2VuZCBtZXNzYWdlIGV2ZXJ5IHNlY29uZFxyXG4gICAgICAgIHNldEludGVydmFsKCgpID0+IHtcclxuICAgICAgICAgICAgYm94LnByb2Nlc3NTaW0odGltZVN0ZXApO1xyXG4gICAgICAgICAgICBwb3N0TWVzc2FnZShib3gsIHVuZGVmaW5lZCk7XHJcblxyXG4gICAgICAgIH0sIHRpbWVTdGVwKTsgLy91cGRhdGUgZXZlcnkgMTAwIG1zLlxyXG4gICAgfVxyXG5cclxuICAgIC8vaWdub3JlXHJcbn1cclxuXHJcblxyXG4iXSwic291cmNlUm9vdCI6IiJ9