//simplifying assumption
//everything has the same mass
//otherwise needed to take into account mass
interface circle {
    x: number,
    y: number,
    radius: number
};

interface velocity {
    vx: number,
    vy: number
};

interface vector2D {
    x: number,
    y: number
}
//Check if two circles overlap
function circleCollision(ball1: circle, ball2: circle): boolean {
    let deltaX = ball2.x - ball1.x;
    let deltaY = ball2.y - ball1.y;
    let distance = Math.sqrt(deltaX ** 2 + deltaY ** 2);// '**' is the power operator 
    let touchingDistance = ball1.radius + ball2.radius;
    return distance <= touchingDistance;

};

function vparallel(actionLine: vector2D, ball: velocity): velocity {
    let sum = actionLine.x * ball.vx +  actionLine.y * ball.vy;
    return {
        vx: actionLine.x * sum,
        vy: actionLine.y * sum,
    }

}
function vperpendicular(actionLine: vector2D, ball: velocity): velocity {
    let vpara = vparallel(actionLine, ball);
    return {
        vx: ball.vx - vpara.vx,
        vy: ball.vy - vpara.vy
    };
}
//Exchange v along the line of action of the two balls.
function momentumTransfer(ball1: ball, ball2: ball) {
    let deltaX = ball1.x - ball2.x;
    let deltaY = ball1.y - ball2.y;
    let distance = Math.max(Math.sqrt(deltaX ** 2 + deltaY ** 2),0.1); //avoid nan
    
    let unitVector = { x: deltaX / distance, y: deltaY / distance };

    let vpara1 = vparallel(unitVector, ball1);
    let vpara2 = vparallel(unitVector, ball2);
    let vperp1 = vperpendicular(unitVector, ball2);
    let vperp2 = vperpendicular(unitVector, ball2);

    //Balls exhange parallel components
    ball1.vx = vperp1.vx + vpara2.vx;
    ball2.vx = vperp2.vx + vpara1.vx;

    ball1.vy = vperp1.vy + vpara2.vy;
    ball2.vy = vperp2.vy + vpara1.vy;
}


class Ball {
    constructor(public x: number, public y: number, public radius: number, public vx: number, public vy: number) { };
    clone() {
        return new Ball(this.x, this.y, this.radius, this.vx, this.vy);
    }
    //Does the physics when two circles collide
    collide(target: Ball) {
        //Check if balls collide
        if (circleCollision(this, target)) {
            //Do transfer of momentum
            momentumTransfer(this, target);
        }
    }
    //Does the position update when two balls collide.
    update(timestep: number) {
        this.x += timestep * this.vx;
        this.y += timestep * this.vy;
    }
    reflectX() {
        this.vx = -this.vx;
    }
    reflectY() {
        this.vy = -this.vy;
    }
};

class Box {
    balls: Array<Ball> = [];
    constructor(private maxX: number, private maxY: number) { }
    processSim(timeStep : number){
        this.updateAll(timeStep);
        this.checkAllCollisions();  
        this.checkBounds();
    }
    addBall(ball: Ball) {
        //add by reference to the array, if it's not already in there
        if (!this.balls.find((value) => { return value === ball })) {
            this.balls.push(ball);
        }
    }
    updateAll(timeStep: number) {
        this.balls.forEach((ball: Ball) => {
            ball.update(timeStep);
        })
    }
    checkAllCollisions() {
        //This is about the only time an indexed for loop makes sense
        for (let i = 0; i < this.balls.length; i += 1) {
            for (let j = i + 1; j < this.balls.length; j += 1) {
                this.balls[i].collide(this.balls[j]);
            }
        }
    }
    checkBounds() {
        this.balls.forEach((ball) => {
            //I guess this could have been a helper function
            if (ball.x < 0 || ball.x > this.maxX) {
                ball.reflectX();
                ball.x = Math.max(ball.x, 0);
                ball.x = Math.min(this.maxX, ball.x);
            }
            if (ball.y < 0 || ball.y > this.maxY) {
                ball.reflectY();
                ball.y = Math.max(ball.y, 0);
                ball.y = Math.min(this.maxY, ball.y);
            }
        })
    }

  
}

//Default symbol exports
//makes it look like a module
export {
    Ball, Box
}