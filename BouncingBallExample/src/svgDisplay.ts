
export class svgBallDisplay{
    circleElements : any = [];
    constructor(public maxX:number,public maxY:number,private balls: any){
        const xmlns = "http://www.w3.org/2000/svg";
           //setup an SVG
        let svgElement = document.querySelector("svg");
        balls.forEach(ball => {
          let newCircle = document.createElementNS(xmlns,"circle");
          newCircle.setAttributeNS(null,"cx", ball.x );
          newCircle.setAttributeNS(null,"cy", ball.y );
          newCircle.setAttributeNS(null,"r", ball.radius);
          newCircle.setAttributeNS(null,"stroke", "black");
          newCircle.setAttributeNS(null,"stroke-width","0.5");
          newCircle.setAttributeNS(null,"fill", "red");
       
          svgElement.appendChild(newCircle);           
          this.circleElements.push(newCircle);
        });
      
    }
    update(balls){   
        for(let i =0; i < this.circleElements.length; i += 1){
            this.circleElements[i].setAttributeNS(null,"cx", balls[i].x );
            this.circleElements[i].setAttributeNS(null,"cy", balls[i].y );
        }
                        
        });
    }
}