import { svgBallDisplay } from "./svgDisplay";



let webWorker = new Worker('worker.js');


let message = {
    x : 1000,
    y: 1000,
    circles : [{
        x: 100, y:100, radius: 5
    }]
}
for(let i = 0; i < 10; i++){
    message.circles.push({
        x: 1000*Math.random(), y:1000*Math.random(), radius: 5
    })
}
webWorker.postMessage(message, undefined)
let svgBox = new svgBallDisplay(message.x, message.y, message.circles);
webWorker.onmessage = (message) => {
    svgBox.update(message.data.balls);
   }
   