import * as physics from './physics';
//set onmessage to receive messages
//pstMesaage to send messages

let box = undefined;

onmessage = (message: any) => {
    console.log(message.data);
    if (!box) {
        box = new physics.Box(message.data.x, message.data.y);
        message.data.circles.forEach(ball => {
            let vx = 0.1 * Math.random();
            let vy = 0.1 * Math.random();
            box.addBall(new physics.Ball(ball.x, ball.y, ball.radius, vx, vy))
        });

        //send message every second
        setInterval(() => {
            box.processSim(timeStep);
            postMessage(box, undefined);

        }, timeStep); //update every 100 ms.
    }

    //ignore
}

const timeStep = 100;
