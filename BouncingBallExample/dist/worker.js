/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./worker/worker.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./worker/physics.ts":
/*!***************************!*\
  !*** ./worker/physics.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.__esModule = true;
;
;
//Check if two circles overlap
function circleCollision(ball1, ball2) {
    var deltaX = ball2.x - ball1.x;
    var deltaY = ball2.y - ball1.y;
    var distance = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2)); // '**' is the power operator 
    var touchingDistance = ball1.radius + ball2.radius;
    return distance <= touchingDistance;
}
;
function vparallel(actionLine, ball) {
    var sum = actionLine.x * ball.vx + actionLine.y * ball.vy;
    return {
        vx: actionLine.x * sum,
        vy: actionLine.y * sum
    };
}
function vperpendicular(actionLine, ball) {
    var vpara = vparallel(actionLine, ball);
    return {
        vx: ball.vx - vpara.vx,
        vy: ball.vy - vpara.vy
    };
}
//Exchange v along the line of action of the two balls.
function momentumTransfer(ball1, ball2) {
    var deltaX = ball1.x - ball2.x;
    var deltaY = ball1.y - ball2.y;
    var distance = Math.max(Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2)), 0.1); //avoid nan
    var unitVector = { x: deltaX / distance, y: deltaY / distance };
    var vpara1 = vparallel(unitVector, ball1);
    var vpara2 = vparallel(unitVector, ball2);
    var vperp1 = vperpendicular(unitVector, ball2);
    var vperp2 = vperpendicular(unitVector, ball2);
    //Balls exhange parallel components
    ball1.vx = vperp1.vx + vpara2.vx;
    ball2.vx = vperp2.vx + vpara1.vx;
    ball1.vy = vperp1.vy + vpara2.vy;
    ball2.vy = vperp2.vy + vpara1.vy;
}
var Ball = /** @class */ (function () {
    function Ball(x, y, radius, vx, vy) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.vx = vx;
        this.vy = vy;
    }
    ;
    Ball.prototype.clone = function () {
        return new Ball(this.x, this.y, this.radius, this.vx, this.vy);
    };
    //Does the physics when two circles collide
    Ball.prototype.collide = function (target) {
        //Check if balls collide
        if (circleCollision(this, target)) {
            //Do transfer of momentum
            momentumTransfer(this, target);
        }
    };
    //Does the position update when two balls collide.
    Ball.prototype.update = function (timestep) {
        this.x += timestep * this.vx;
        this.y += timestep * this.vy;
    };
    Ball.prototype.reflectX = function () {
        this.vx = -this.vx;
    };
    Ball.prototype.reflectY = function () {
        this.vy = -this.vy;
    };
    return Ball;
}());
exports.Ball = Ball;
;
var Box = /** @class */ (function () {
    function Box(maxX, maxY) {
        this.maxX = maxX;
        this.maxY = maxY;
        this.balls = [];
    }
    Box.prototype.processSim = function (timeStep) {
        this.updateAll(timeStep);
        this.checkAllCollisions();
        this.checkBounds();
    };
    Box.prototype.addBall = function (ball) {
        //add by reference to the array, if it's not already in there
        if (!this.balls.find(function (value) { return value === ball; })) {
            this.balls.push(ball);
        }
    };
    Box.prototype.updateAll = function (timeStep) {
        this.balls.forEach(function (ball) {
            ball.update(timeStep);
        });
    };
    Box.prototype.checkAllCollisions = function () {
        //This is about the only time an indexed for loop makes sense
        for (var i = 0; i < this.balls.length; i += 1) {
            for (var j = i + 1; j < this.balls.length; j += 1) {
                this.balls[i].collide(this.balls[j]);
            }
        }
    };
    Box.prototype.checkBounds = function () {
        var _this = this;
        this.balls.forEach(function (ball) {
            //I guess this could have been a helper function
            if (ball.x < 0 || ball.x > _this.maxX) {
                ball.reflectX();
                ball.x = Math.max(ball.x, 0);
                ball.x = Math.min(_this.maxX, ball.x);
            }
            if (ball.y < 0 || ball.y > _this.maxY) {
                ball.reflectY();
                ball.y = Math.max(ball.y, 0);
                ball.y = Math.min(_this.maxY, ball.y);
            }
        });
    };
    return Box;
}());
exports.Box = Box;


/***/ }),

/***/ "./worker/worker.ts":
/*!**************************!*\
  !*** ./worker/worker.ts ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.__esModule = true;
var physics = __webpack_require__(/*! ./physics */ "./worker/physics.ts");
//set onmessage to receive messages
//pstMesaage to send messages
var box = undefined;
onmessage = function (message) {
    console.log(message.data);
    if (!box) {
        box = new physics.Box(message.data.x, message.data.y);
        message.data.circles.forEach(function (ball) {
            var vx = 0.1 * Math.random();
            var vy = 0.1 * Math.random();
            box.addBall(new physics.Ball(ball.x, ball.y, ball.radius, vx, vy));
        });
        //send message every second
        setInterval(function () {
            box.processSim(timeStep);
            postMessage(box, undefined);
        }, timeStep); //update every 100 ms.
    }
    //ignore
};
var timeStep = 100;


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vd29ya2VyL3BoeXNpY3MudHMiLCJ3ZWJwYWNrOi8vLy4vd29ya2VyL3dvcmtlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxrREFBMEMsZ0NBQWdDO0FBQzFFO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZ0VBQXdELGtCQUFrQjtBQUMxRTtBQUNBLHlEQUFpRCxjQUFjO0FBQy9EOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpREFBeUMsaUNBQWlDO0FBQzFFLHdIQUFnSCxtQkFBbUIsRUFBRTtBQUNySTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOzs7QUFHQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7QUMzRUMsQ0FBQztBQUtELENBQUM7QUFNRiw4QkFBOEI7QUFDOUIseUJBQXlCLEtBQWEsRUFBRSxLQUFhO0lBQ2pELElBQUksTUFBTSxHQUFHLEtBQUssQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUMvQixJQUFJLE1BQU0sR0FBRyxLQUFLLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDL0IsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFNLEVBQUksQ0FBQyxJQUFHLGVBQU0sRUFBSSxDQUFDLEVBQUMsQ0FBQywrQkFBOEI7SUFDbEYsSUFBSSxnQkFBZ0IsR0FBRyxLQUFLLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUM7SUFDbkQsT0FBTyxRQUFRLElBQUksZ0JBQWdCLENBQUM7QUFFeEMsQ0FBQztBQUFBLENBQUM7QUFFRixtQkFBbUIsVUFBb0IsRUFBRSxJQUFjO0lBQ25ELElBQUksR0FBRyxHQUFHLFVBQVUsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsR0FBSSxVQUFVLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUM7SUFDM0QsT0FBTztRQUNILEVBQUUsRUFBRSxVQUFVLENBQUMsQ0FBQyxHQUFHLEdBQUc7UUFDdEIsRUFBRSxFQUFFLFVBQVUsQ0FBQyxDQUFDLEdBQUcsR0FBRztLQUN6QjtBQUVMLENBQUM7QUFDRCx3QkFBd0IsVUFBb0IsRUFBRSxJQUFjO0lBQ3hELElBQUksS0FBSyxHQUFHLFNBQVMsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDeEMsT0FBTztRQUNILEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRSxHQUFHLEtBQUssQ0FBQyxFQUFFO1FBQ3RCLEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRSxHQUFHLEtBQUssQ0FBQyxFQUFFO0tBQ3pCLENBQUM7QUFDTixDQUFDO0FBQ0QsdURBQXVEO0FBQ3ZELDBCQUEwQixLQUFXLEVBQUUsS0FBVztJQUM5QyxJQUFJLE1BQU0sR0FBRyxLQUFLLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDL0IsSUFBSSxNQUFNLEdBQUcsS0FBSyxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBQy9CLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFNLEVBQUksQ0FBQyxJQUFHLGVBQU0sRUFBSSxDQUFDLEVBQUMsRUFBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLFdBQVc7SUFFOUUsSUFBSSxVQUFVLEdBQUcsRUFBRSxDQUFDLEVBQUUsTUFBTSxHQUFHLFFBQVEsRUFBRSxDQUFDLEVBQUUsTUFBTSxHQUFHLFFBQVEsRUFBRSxDQUFDO0lBRWhFLElBQUksTUFBTSxHQUFHLFNBQVMsQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDMUMsSUFBSSxNQUFNLEdBQUcsU0FBUyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUMxQyxJQUFJLE1BQU0sR0FBRyxjQUFjLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQy9DLElBQUksTUFBTSxHQUFHLGNBQWMsQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFFL0MsbUNBQW1DO0lBQ25DLEtBQUssQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDO0lBQ2pDLEtBQUssQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDO0lBRWpDLEtBQUssQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDO0lBQ2pDLEtBQUssQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDO0FBQ3JDLENBQUM7QUFHRDtJQUNJLGNBQW1CLENBQVMsRUFBUyxDQUFTLEVBQVMsTUFBYyxFQUFTLEVBQVUsRUFBUyxFQUFVO1FBQXhGLE1BQUMsR0FBRCxDQUFDLENBQVE7UUFBUyxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQVMsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUFTLE9BQUUsR0FBRixFQUFFLENBQVE7UUFBUyxPQUFFLEdBQUYsRUFBRSxDQUFRO0lBQUksQ0FBQztJQUFBLENBQUM7SUFDakgsb0JBQUssR0FBTDtRQUNJLE9BQU8sSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDbkUsQ0FBQztJQUNELDJDQUEyQztJQUMzQyxzQkFBTyxHQUFQLFVBQVEsTUFBWTtRQUNoQix3QkFBd0I7UUFDeEIsSUFBSSxlQUFlLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQy9CLHlCQUF5QjtZQUN6QixnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7U0FDbEM7SUFDTCxDQUFDO0lBQ0Qsa0RBQWtEO0lBQ2xELHFCQUFNLEdBQU4sVUFBTyxRQUFnQjtRQUNuQixJQUFJLENBQUMsQ0FBQyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDO1FBQzdCLElBQUksQ0FBQyxDQUFDLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUM7SUFDakMsQ0FBQztJQUNELHVCQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBQ0QsdUJBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO0lBQ3ZCLENBQUM7SUFDTCxXQUFDO0FBQUQsQ0FBQztBQW1ERyxvQkFBSTtBQW5EUCxDQUFDO0FBRUY7SUFFSSxhQUFvQixJQUFZLEVBQVUsSUFBWTtRQUFsQyxTQUFJLEdBQUosSUFBSSxDQUFRO1FBQVUsU0FBSSxHQUFKLElBQUksQ0FBUTtRQUR0RCxVQUFLLEdBQWdCLEVBQUUsQ0FBQztJQUNrQyxDQUFDO0lBQzNELHdCQUFVLEdBQVYsVUFBVyxRQUFpQjtRQUN4QixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBQ0QscUJBQU8sR0FBUCxVQUFRLElBQVU7UUFDZCw2REFBNkQ7UUFDN0QsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQUMsS0FBSyxJQUFPLE9BQU8sS0FBSyxLQUFLLElBQUksRUFBQyxDQUFDLENBQUMsRUFBRTtZQUN4RCxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN6QjtJQUNMLENBQUM7SUFDRCx1QkFBUyxHQUFULFVBQVUsUUFBZ0I7UUFDdEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFVO1lBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDMUIsQ0FBQyxDQUFDO0lBQ04sQ0FBQztJQUNELGdDQUFrQixHQUFsQjtRQUNJLDZEQUE2RDtRQUM3RCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUMzQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQy9DLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUN4QztTQUNKO0lBQ0wsQ0FBQztJQUNELHlCQUFXLEdBQVg7UUFBQSxpQkFjQztRQWJHLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQUMsSUFBSTtZQUNwQixnREFBZ0Q7WUFDaEQsSUFBSSxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxHQUFHLEtBQUksQ0FBQyxJQUFJLEVBQUU7Z0JBQ2xDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDaEIsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQzdCLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUN4QztZQUNELElBQUksSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLENBQUMsR0FBRyxLQUFJLENBQUMsSUFBSSxFQUFFO2dCQUNsQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ2hCLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUM3QixJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDeEM7UUFDTCxDQUFDLENBQUM7SUFDTixDQUFDO0lBR0wsVUFBQztBQUFELENBQUM7QUFLUyxrQkFBRzs7Ozs7Ozs7Ozs7Ozs7O0FDNUliLDBFQUFxQztBQUNyQyxtQ0FBbUM7QUFDbkMsNkJBQTZCO0FBRTdCLElBQUksR0FBRyxHQUFHLFNBQVMsQ0FBQztBQUVwQixTQUFTLEdBQUcsVUFBQyxPQUFZO0lBQ3JCLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFCLElBQUksQ0FBQyxHQUFHLEVBQUU7UUFDTixHQUFHLEdBQUcsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDdEQsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLGNBQUk7WUFDN0IsSUFBSSxFQUFFLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUM3QixJQUFJLEVBQUUsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQzdCLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUN0RSxDQUFDLENBQUMsQ0FBQztRQUVILDJCQUEyQjtRQUMzQixXQUFXLENBQUM7WUFDUixHQUFHLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3pCLFdBQVcsQ0FBQyxHQUFHLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFFaEMsQ0FBQyxFQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUMsc0JBQXNCO0tBQ3ZDO0lBRUQsUUFBUTtBQUNaLENBQUM7QUFFRCxJQUFNLFFBQVEsR0FBRyxHQUFHLENBQUMiLCJmaWxlIjoid29ya2VyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi93b3JrZXIvd29ya2VyLnRzXCIpO1xuIiwiLy9zaW1wbGlmeWluZyBhc3N1bXB0aW9uXHJcbi8vZXZlcnl0aGluZyBoYXMgdGhlIHNhbWUgbWFzc1xyXG4vL290aGVyd2lzZSBuZWVkZWQgdG8gdGFrZSBpbnRvIGFjY291bnQgbWFzc1xyXG5pbnRlcmZhY2UgY2lyY2xlIHtcclxuICAgIHg6IG51bWJlcixcclxuICAgIHk6IG51bWJlcixcclxuICAgIHJhZGl1czogbnVtYmVyXHJcbn07XHJcblxyXG5pbnRlcmZhY2UgdmVsb2NpdHkge1xyXG4gICAgdng6IG51bWJlcixcclxuICAgIHZ5OiBudW1iZXJcclxufTtcclxuXHJcbmludGVyZmFjZSB2ZWN0b3IyRCB7XHJcbiAgICB4OiBudW1iZXIsXHJcbiAgICB5OiBudW1iZXJcclxufVxyXG4vL0NoZWNrIGlmIHR3byBjaXJjbGVzIG92ZXJsYXBcclxuZnVuY3Rpb24gY2lyY2xlQ29sbGlzaW9uKGJhbGwxOiBjaXJjbGUsIGJhbGwyOiBjaXJjbGUpOiBib29sZWFuIHtcclxuICAgIGxldCBkZWx0YVggPSBiYWxsMi54IC0gYmFsbDEueDtcclxuICAgIGxldCBkZWx0YVkgPSBiYWxsMi55IC0gYmFsbDEueTtcclxuICAgIGxldCBkaXN0YW5jZSA9IE1hdGguc3FydChkZWx0YVggKiogMiArIGRlbHRhWSAqKiAyKTsvLyAnKionIGlzIHRoZSBwb3dlciBvcGVyYXRvciBcclxuICAgIGxldCB0b3VjaGluZ0Rpc3RhbmNlID0gYmFsbDEucmFkaXVzICsgYmFsbDIucmFkaXVzO1xyXG4gICAgcmV0dXJuIGRpc3RhbmNlIDw9IHRvdWNoaW5nRGlzdGFuY2U7XHJcblxyXG59O1xyXG5cclxuZnVuY3Rpb24gdnBhcmFsbGVsKGFjdGlvbkxpbmU6IHZlY3RvcjJELCBiYWxsOiB2ZWxvY2l0eSk6IHZlbG9jaXR5IHtcclxuICAgIGxldCBzdW0gPSBhY3Rpb25MaW5lLnggKiBiYWxsLnZ4ICsgIGFjdGlvbkxpbmUueSAqIGJhbGwudnk7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIHZ4OiBhY3Rpb25MaW5lLnggKiBzdW0sXHJcbiAgICAgICAgdnk6IGFjdGlvbkxpbmUueSAqIHN1bSxcclxuICAgIH1cclxuXHJcbn1cclxuZnVuY3Rpb24gdnBlcnBlbmRpY3VsYXIoYWN0aW9uTGluZTogdmVjdG9yMkQsIGJhbGw6IHZlbG9jaXR5KTogdmVsb2NpdHkge1xyXG4gICAgbGV0IHZwYXJhID0gdnBhcmFsbGVsKGFjdGlvbkxpbmUsIGJhbGwpO1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICB2eDogYmFsbC52eCAtIHZwYXJhLnZ4LFxyXG4gICAgICAgIHZ5OiBiYWxsLnZ5IC0gdnBhcmEudnlcclxuICAgIH07XHJcbn1cclxuLy9FeGNoYW5nZSB2IGFsb25nIHRoZSBsaW5lIG9mIGFjdGlvbiBvZiB0aGUgdHdvIGJhbGxzLlxyXG5mdW5jdGlvbiBtb21lbnR1bVRyYW5zZmVyKGJhbGwxOiBiYWxsLCBiYWxsMjogYmFsbCkge1xyXG4gICAgbGV0IGRlbHRhWCA9IGJhbGwxLnggLSBiYWxsMi54O1xyXG4gICAgbGV0IGRlbHRhWSA9IGJhbGwxLnkgLSBiYWxsMi55O1xyXG4gICAgbGV0IGRpc3RhbmNlID0gTWF0aC5tYXgoTWF0aC5zcXJ0KGRlbHRhWCAqKiAyICsgZGVsdGFZICoqIDIpLDAuMSk7IC8vYXZvaWQgbmFuXHJcbiAgICBcclxuICAgIGxldCB1bml0VmVjdG9yID0geyB4OiBkZWx0YVggLyBkaXN0YW5jZSwgeTogZGVsdGFZIC8gZGlzdGFuY2UgfTtcclxuXHJcbiAgICBsZXQgdnBhcmExID0gdnBhcmFsbGVsKHVuaXRWZWN0b3IsIGJhbGwxKTtcclxuICAgIGxldCB2cGFyYTIgPSB2cGFyYWxsZWwodW5pdFZlY3RvciwgYmFsbDIpO1xyXG4gICAgbGV0IHZwZXJwMSA9IHZwZXJwZW5kaWN1bGFyKHVuaXRWZWN0b3IsIGJhbGwyKTtcclxuICAgIGxldCB2cGVycDIgPSB2cGVycGVuZGljdWxhcih1bml0VmVjdG9yLCBiYWxsMik7XHJcblxyXG4gICAgLy9CYWxscyBleGhhbmdlIHBhcmFsbGVsIGNvbXBvbmVudHNcclxuICAgIGJhbGwxLnZ4ID0gdnBlcnAxLnZ4ICsgdnBhcmEyLnZ4O1xyXG4gICAgYmFsbDIudnggPSB2cGVycDIudnggKyB2cGFyYTEudng7XHJcblxyXG4gICAgYmFsbDEudnkgPSB2cGVycDEudnkgKyB2cGFyYTIudnk7XHJcbiAgICBiYWxsMi52eSA9IHZwZXJwMi52eSArIHZwYXJhMS52eTtcclxufVxyXG5cclxuXHJcbmNsYXNzIEJhbGwge1xyXG4gICAgY29uc3RydWN0b3IocHVibGljIHg6IG51bWJlciwgcHVibGljIHk6IG51bWJlciwgcHVibGljIHJhZGl1czogbnVtYmVyLCBwdWJsaWMgdng6IG51bWJlciwgcHVibGljIHZ5OiBudW1iZXIpIHsgfTtcclxuICAgIGNsb25lKCkge1xyXG4gICAgICAgIHJldHVybiBuZXcgQmFsbCh0aGlzLngsIHRoaXMueSwgdGhpcy5yYWRpdXMsIHRoaXMudngsIHRoaXMudnkpO1xyXG4gICAgfVxyXG4gICAgLy9Eb2VzIHRoZSBwaHlzaWNzIHdoZW4gdHdvIGNpcmNsZXMgY29sbGlkZVxyXG4gICAgY29sbGlkZSh0YXJnZXQ6IEJhbGwpIHtcclxuICAgICAgICAvL0NoZWNrIGlmIGJhbGxzIGNvbGxpZGVcclxuICAgICAgICBpZiAoY2lyY2xlQ29sbGlzaW9uKHRoaXMsIHRhcmdldCkpIHtcclxuICAgICAgICAgICAgLy9EbyB0cmFuc2ZlciBvZiBtb21lbnR1bVxyXG4gICAgICAgICAgICBtb21lbnR1bVRyYW5zZmVyKHRoaXMsIHRhcmdldCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLy9Eb2VzIHRoZSBwb3NpdGlvbiB1cGRhdGUgd2hlbiB0d28gYmFsbHMgY29sbGlkZS5cclxuICAgIHVwZGF0ZSh0aW1lc3RlcDogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy54ICs9IHRpbWVzdGVwICogdGhpcy52eDtcclxuICAgICAgICB0aGlzLnkgKz0gdGltZXN0ZXAgKiB0aGlzLnZ5O1xyXG4gICAgfVxyXG4gICAgcmVmbGVjdFgoKSB7XHJcbiAgICAgICAgdGhpcy52eCA9IC10aGlzLnZ4O1xyXG4gICAgfVxyXG4gICAgcmVmbGVjdFkoKSB7XHJcbiAgICAgICAgdGhpcy52eSA9IC10aGlzLnZ5O1xyXG4gICAgfVxyXG59O1xyXG5cclxuY2xhc3MgQm94IHtcclxuICAgIGJhbGxzOiBBcnJheTxCYWxsPiA9IFtdO1xyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBtYXhYOiBudW1iZXIsIHByaXZhdGUgbWF4WTogbnVtYmVyKSB7IH1cclxuICAgIHByb2Nlc3NTaW0odGltZVN0ZXAgOiBudW1iZXIpe1xyXG4gICAgICAgIHRoaXMudXBkYXRlQWxsKHRpbWVTdGVwKTtcclxuICAgICAgICB0aGlzLmNoZWNrQWxsQ29sbGlzaW9ucygpOyAgXHJcbiAgICAgICAgdGhpcy5jaGVja0JvdW5kcygpO1xyXG4gICAgfVxyXG4gICAgYWRkQmFsbChiYWxsOiBCYWxsKSB7XHJcbiAgICAgICAgLy9hZGQgYnkgcmVmZXJlbmNlIHRvIHRoZSBhcnJheSwgaWYgaXQncyBub3QgYWxyZWFkeSBpbiB0aGVyZVxyXG4gICAgICAgIGlmICghdGhpcy5iYWxscy5maW5kKCh2YWx1ZSkgPT4geyByZXR1cm4gdmFsdWUgPT09IGJhbGwgfSkpIHtcclxuICAgICAgICAgICAgdGhpcy5iYWxscy5wdXNoKGJhbGwpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIHVwZGF0ZUFsbCh0aW1lU3RlcDogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5iYWxscy5mb3JFYWNoKChiYWxsOiBCYWxsKSA9PiB7XHJcbiAgICAgICAgICAgIGJhbGwudXBkYXRlKHRpbWVTdGVwKTtcclxuICAgICAgICB9KVxyXG4gICAgfVxyXG4gICAgY2hlY2tBbGxDb2xsaXNpb25zKCkge1xyXG4gICAgICAgIC8vVGhpcyBpcyBhYm91dCB0aGUgb25seSB0aW1lIGFuIGluZGV4ZWQgZm9yIGxvb3AgbWFrZXMgc2Vuc2VcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuYmFsbHMubGVuZ3RoOyBpICs9IDEpIHtcclxuICAgICAgICAgICAgZm9yIChsZXQgaiA9IGkgKyAxOyBqIDwgdGhpcy5iYWxscy5sZW5ndGg7IGogKz0gMSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5iYWxsc1tpXS5jb2xsaWRlKHRoaXMuYmFsbHNbal0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgY2hlY2tCb3VuZHMoKSB7XHJcbiAgICAgICAgdGhpcy5iYWxscy5mb3JFYWNoKChiYWxsKSA9PiB7XHJcbiAgICAgICAgICAgIC8vSSBndWVzcyB0aGlzIGNvdWxkIGhhdmUgYmVlbiBhIGhlbHBlciBmdW5jdGlvblxyXG4gICAgICAgICAgICBpZiAoYmFsbC54IDwgMCB8fCBiYWxsLnggPiB0aGlzLm1heFgpIHtcclxuICAgICAgICAgICAgICAgIGJhbGwucmVmbGVjdFgoKTtcclxuICAgICAgICAgICAgICAgIGJhbGwueCA9IE1hdGgubWF4KGJhbGwueCwgMCk7XHJcbiAgICAgICAgICAgICAgICBiYWxsLnggPSBNYXRoLm1pbih0aGlzLm1heFgsIGJhbGwueCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKGJhbGwueSA8IDAgfHwgYmFsbC55ID4gdGhpcy5tYXhZKSB7XHJcbiAgICAgICAgICAgICAgICBiYWxsLnJlZmxlY3RZKCk7XHJcbiAgICAgICAgICAgICAgICBiYWxsLnkgPSBNYXRoLm1heChiYWxsLnksIDApO1xyXG4gICAgICAgICAgICAgICAgYmFsbC55ID0gTWF0aC5taW4odGhpcy5tYXhZLCBiYWxsLnkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSlcclxuICAgIH1cclxuXHJcbiAgXHJcbn1cclxuXHJcbi8vRGVmYXVsdCBzeW1ib2wgZXhwb3J0c1xyXG4vL21ha2VzIGl0IGxvb2sgbGlrZSBhIG1vZHVsZVxyXG5leHBvcnQge1xyXG4gICAgQmFsbCwgQm94XHJcbn0iLCJpbXBvcnQgKiBhcyBwaHlzaWNzIGZyb20gJy4vcGh5c2ljcyc7XHJcbi8vc2V0IG9ubWVzc2FnZSB0byByZWNlaXZlIG1lc3NhZ2VzXHJcbi8vcHN0TWVzYWFnZSB0byBzZW5kIG1lc3NhZ2VzXHJcblxyXG5sZXQgYm94ID0gdW5kZWZpbmVkO1xyXG5cclxub25tZXNzYWdlID0gKG1lc3NhZ2U6IGFueSkgPT4ge1xyXG4gICAgY29uc29sZS5sb2cobWVzc2FnZS5kYXRhKTtcclxuICAgIGlmICghYm94KSB7XHJcbiAgICAgICAgYm94ID0gbmV3IHBoeXNpY3MuQm94KG1lc3NhZ2UuZGF0YS54LCBtZXNzYWdlLmRhdGEueSk7XHJcbiAgICAgICAgbWVzc2FnZS5kYXRhLmNpcmNsZXMuZm9yRWFjaChiYWxsID0+IHtcclxuICAgICAgICAgICAgbGV0IHZ4ID0gMC4xICogTWF0aC5yYW5kb20oKTtcclxuICAgICAgICAgICAgbGV0IHZ5ID0gMC4xICogTWF0aC5yYW5kb20oKTtcclxuICAgICAgICAgICAgYm94LmFkZEJhbGwobmV3IHBoeXNpY3MuQmFsbChiYWxsLngsIGJhbGwueSwgYmFsbC5yYWRpdXMsIHZ4LCB2eSkpXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIC8vc2VuZCBtZXNzYWdlIGV2ZXJ5IHNlY29uZFxyXG4gICAgICAgIHNldEludGVydmFsKCgpID0+IHtcclxuICAgICAgICAgICAgYm94LnByb2Nlc3NTaW0odGltZVN0ZXApO1xyXG4gICAgICAgICAgICBwb3N0TWVzc2FnZShib3gsIHVuZGVmaW5lZCk7XHJcblxyXG4gICAgICAgIH0sIHRpbWVTdGVwKTsgLy91cGRhdGUgZXZlcnkgMTAwIG1zLlxyXG4gICAgfVxyXG5cclxuICAgIC8vaWdub3JlXHJcbn1cclxuXHJcbmNvbnN0IHRpbWVTdGVwID0gMTAwO1xyXG4iXSwic291cmNlUm9vdCI6IiJ9