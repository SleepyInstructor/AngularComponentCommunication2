/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.ts":
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.__esModule = true;
var svgDisplay_1 = __webpack_require__(/*! ./svgDisplay */ "./src/svgDisplay.ts");
var webWorker = new Worker('worker.js');
var message = {
    x: 1000,
    y: 1000,
    circles: [{
            x: 100, y: 100, radius: 5
        }]
};
for (var i = 0; i < 10; i++) {
    message.circles.push({
        x: 1000 * Math.random(), y: 1000 * Math.random(), radius: 5
    });
}
webWorker.postMessage(message, undefined);
var svgBox = new svgDisplay_1.svgBallDisplay(message.x, message.y, message.circles);
webWorker.onmessage = function (message) {
    svgBox.update(message.data.balls);
};


/***/ }),

/***/ "./src/svgDisplay.ts":
/*!***************************!*\
  !*** ./src/svgDisplay.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.__esModule = true;
var svgBallDisplay = /** @class */ (function () {
    function svgBallDisplay(maxX, maxY, balls) {
        var _this = this;
        this.maxX = maxX;
        this.maxY = maxY;
        this.balls = balls;
        this.circleElements = [];
        var xmlns = "http://www.w3.org/2000/svg";
        //setup an SVG
        var svgElement = document.querySelector("svg");
        balls.forEach(function (ball) {
            var newCircle = document.createElementNS(xmlns, "circle");
            newCircle.setAttributeNS(null, "cx", ball.x);
            newCircle.setAttributeNS(null, "cy", ball.y);
            newCircle.setAttributeNS(null, "r", ball.radius);
            newCircle.setAttributeNS(null, "stroke", "black");
            newCircle.setAttributeNS(null, "stroke-width", "0.5");
            newCircle.setAttributeNS(null, "fill", "red");
            svgElement.appendChild(newCircle);
            _this.circleElements.push(newCircle);
        });
    }
    svgBallDisplay.prototype.update = function (balls) {
        for (var i = 0; i < this.circleElements.length; i += 1) {
            this.circleElements[i].setAttributeNS(null, "cx", balls[i].x);
            this.circleElements[i].setAttributeNS(null, "cy", balls[i].y);
        }
    };
    ;
    return svgBallDisplay;
}());
exports.svgBallDisplay = svgBallDisplay;


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LnRzIiwid2VicGFjazovLy8uL3NyYy9zdmdEaXNwbGF5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7OztBQ2xGQSxrRkFBOEM7QUFJOUMsSUFBSSxTQUFTLEdBQUcsSUFBSSxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUM7QUFHeEMsSUFBSSxPQUFPLEdBQUc7SUFDVixDQUFDLEVBQUcsSUFBSTtJQUNSLENBQUMsRUFBRSxJQUFJO0lBQ1AsT0FBTyxFQUFHLENBQUM7WUFDUCxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBQyxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUM7U0FDM0IsQ0FBQztDQUNMO0FBQ0QsS0FBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQUUsRUFBQztJQUN2QixPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztRQUNqQixDQUFDLEVBQUUsSUFBSSxHQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLEVBQUMsSUFBSSxHQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBRSxNQUFNLEVBQUUsQ0FBQztLQUN6RCxDQUFDO0NBQ0w7QUFDRCxTQUFTLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxTQUFTLENBQUM7QUFDekMsSUFBSSxNQUFNLEdBQUcsSUFBSSwyQkFBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7QUFDdkUsU0FBUyxDQUFDLFNBQVMsR0FBRyxVQUFDLE9BQU87SUFDMUIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0FBQ25DLENBQUM7Ozs7Ozs7Ozs7Ozs7OztBQ3RCSjtJQUVJLHdCQUFtQixJQUFXLEVBQVEsSUFBVyxFQUFTLEtBQVU7UUFBcEUsaUJBaUJDO1FBakJrQixTQUFJLEdBQUosSUFBSSxDQUFPO1FBQVEsU0FBSSxHQUFKLElBQUksQ0FBTztRQUFTLFVBQUssR0FBTCxLQUFLLENBQUs7UUFEcEUsbUJBQWMsR0FBUyxFQUFFLENBQUM7UUFFdEIsSUFBTSxLQUFLLEdBQUcsNEJBQTRCLENBQUM7UUFDeEMsY0FBYztRQUNqQixJQUFJLFVBQVUsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9DLEtBQUssQ0FBQyxPQUFPLENBQUMsY0FBSTtZQUNoQixJQUFJLFNBQVMsR0FBRyxRQUFRLENBQUMsZUFBZSxDQUFDLEtBQUssRUFBQyxRQUFRLENBQUMsQ0FBQztZQUN6RCxTQUFTLENBQUMsY0FBYyxDQUFDLElBQUksRUFBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBRSxDQUFDO1lBQzdDLFNBQVMsQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFFLENBQUM7WUFDN0MsU0FBUyxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNoRCxTQUFTLENBQUMsY0FBYyxDQUFDLElBQUksRUFBQyxRQUFRLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDakQsU0FBUyxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUMsY0FBYyxFQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3BELFNBQVMsQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztZQUU3QyxVQUFVLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ2xDLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3RDLENBQUMsQ0FBQyxDQUFDO0lBRVAsQ0FBQztJQUNELCtCQUFNLEdBQU4sVUFBTyxLQUFLO1FBQ1IsS0FBSSxJQUFJLENBQUMsR0FBRSxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUM7WUFDakQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFFLENBQUM7WUFDOUQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFFLENBQUM7U0FDakU7SUFFRCxDQUFDO0lBQUMsQ0FBQztJQUNQLHFCQUFDO0FBQUQsQ0FBQztBQTNCUSx3Q0FBYyIsImZpbGUiOiJtYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9zcmMvaW5kZXgudHNcIik7XG4iLCJpbXBvcnQgeyBzdmdCYWxsRGlzcGxheSB9IGZyb20gXCIuL3N2Z0Rpc3BsYXlcIjtcclxuXHJcblxyXG5cclxubGV0IHdlYldvcmtlciA9IG5ldyBXb3JrZXIoJ3dvcmtlci5qcycpO1xyXG5cclxuXHJcbmxldCBtZXNzYWdlID0ge1xyXG4gICAgeCA6IDEwMDAsXHJcbiAgICB5OiAxMDAwLFxyXG4gICAgY2lyY2xlcyA6IFt7XHJcbiAgICAgICAgeDogMTAwLCB5OjEwMCwgcmFkaXVzOiA1XHJcbiAgICB9XVxyXG59XHJcbmZvcihsZXQgaSA9IDA7IGkgPCAxMDsgaSsrKXtcclxuICAgIG1lc3NhZ2UuY2lyY2xlcy5wdXNoKHtcclxuICAgICAgICB4OiAxMDAwKk1hdGgucmFuZG9tKCksIHk6MTAwMCpNYXRoLnJhbmRvbSgpLCByYWRpdXM6IDVcclxuICAgIH0pXHJcbn1cclxud2ViV29ya2VyLnBvc3RNZXNzYWdlKG1lc3NhZ2UsIHVuZGVmaW5lZClcclxubGV0IHN2Z0JveCA9IG5ldyBzdmdCYWxsRGlzcGxheShtZXNzYWdlLngsIG1lc3NhZ2UueSwgbWVzc2FnZS5jaXJjbGVzKTtcclxud2ViV29ya2VyLm9ubWVzc2FnZSA9IChtZXNzYWdlKSA9PiB7XHJcbiAgICBzdmdCb3gudXBkYXRlKG1lc3NhZ2UuZGF0YS5iYWxscyk7XHJcbiAgIH1cclxuICAgIiwiXHJcbmV4cG9ydCBjbGFzcyBzdmdCYWxsRGlzcGxheXtcclxuICAgIGNpcmNsZUVsZW1lbnRzIDogYW55ID0gW107XHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgbWF4WDpudW1iZXIscHVibGljIG1heFk6bnVtYmVyLHByaXZhdGUgYmFsbHM6IGFueSl7XHJcbiAgICAgICAgY29uc3QgeG1sbnMgPSBcImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCI7XHJcbiAgICAgICAgICAgLy9zZXR1cCBhbiBTVkdcclxuICAgICAgICBsZXQgc3ZnRWxlbWVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJzdmdcIik7XHJcbiAgICAgICAgYmFsbHMuZm9yRWFjaChiYWxsID0+IHtcclxuICAgICAgICAgIGxldCBuZXdDaXJjbGUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50TlMoeG1sbnMsXCJjaXJjbGVcIik7XHJcbiAgICAgICAgICBuZXdDaXJjbGUuc2V0QXR0cmlidXRlTlMobnVsbCxcImN4XCIsIGJhbGwueCApO1xyXG4gICAgICAgICAgbmV3Q2lyY2xlLnNldEF0dHJpYnV0ZU5TKG51bGwsXCJjeVwiLCBiYWxsLnkgKTtcclxuICAgICAgICAgIG5ld0NpcmNsZS5zZXRBdHRyaWJ1dGVOUyhudWxsLFwiclwiLCBiYWxsLnJhZGl1cyk7XHJcbiAgICAgICAgICBuZXdDaXJjbGUuc2V0QXR0cmlidXRlTlMobnVsbCxcInN0cm9rZVwiLCBcImJsYWNrXCIpO1xyXG4gICAgICAgICAgbmV3Q2lyY2xlLnNldEF0dHJpYnV0ZU5TKG51bGwsXCJzdHJva2Utd2lkdGhcIixcIjAuNVwiKTtcclxuICAgICAgICAgIG5ld0NpcmNsZS5zZXRBdHRyaWJ1dGVOUyhudWxsLFwiZmlsbFwiLCBcInJlZFwiKTtcclxuICAgICAgIFxyXG4gICAgICAgICAgc3ZnRWxlbWVudC5hcHBlbmRDaGlsZChuZXdDaXJjbGUpOyAgICAgICAgICAgXHJcbiAgICAgICAgICB0aGlzLmNpcmNsZUVsZW1lbnRzLnB1c2gobmV3Q2lyY2xlKTtcclxuICAgICAgICB9KTtcclxuICAgICAgXHJcbiAgICB9XHJcbiAgICB1cGRhdGUoYmFsbHMpeyAgIFxyXG4gICAgICAgIGZvcihsZXQgaSA9MDsgaSA8IHRoaXMuY2lyY2xlRWxlbWVudHMubGVuZ3RoOyBpICs9IDEpe1xyXG4gICAgICAgICAgICB0aGlzLmNpcmNsZUVsZW1lbnRzW2ldLnNldEF0dHJpYnV0ZU5TKG51bGwsXCJjeFwiLCBiYWxsc1tpXS54ICk7XHJcbiAgICAgICAgICAgIHRoaXMuY2lyY2xlRWxlbWVudHNbaV0uc2V0QXR0cmlidXRlTlMobnVsbCxcImN5XCIsIGJhbGxzW2ldLnkgKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59Il0sInNvdXJjZVJvb3QiOiIifQ==